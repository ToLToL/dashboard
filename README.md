# Dashboard

Le but de ce projet est de se familiariser avec les technologies webs comme NodeJS, ReactJS et MongoDB à travers la réalisation d’un dashboard.
Pour ce faire, on doit implémenter une app web dont le fonctionnement ressemble à celui de Netvibes.

## Installation

    docker-compose build

## Running

    docker-compose up