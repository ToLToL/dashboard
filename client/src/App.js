import React from 'react';
import Login from './components/Login';
import Register from './components/Register';
import Welcome from './components/Welcome';
import Dashboard from './components/Dashboard/Dashboard';
import { BrowserRouter, Route } from "react-router-dom";


import 'bootstrap/dist/css/bootstrap.min.css';
import './App.css';

function App() {
  return (
    <div className="App">
      <BrowserRouter>
          <Route path="/" exact component={Welcome}/>
          <Route path="/register" exact component={Register} />
          <Route path="/login" exact component={Login} />
          <Route path="/dashboard" exact component={Dashboard} />
      </BrowserRouter>
    </div>
  );
}

export default App;
