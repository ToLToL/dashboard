import React from 'react';
import Grid from '@material-ui/core/Grid';

import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';

import GetTimer from './GetTimer'

import { DrawerContext, BoardContext } from '../Dashboard'

import Video from '../Widgets/Video';
import uuid from 'uuid'

export default function ChannelForm() {
    const [DrawerState, DrawerSetState] = React.useContext(DrawerContext);
    const [BoardState, BoardSetState] = React.useContext(BoardContext);
    const [state, setState] = React.useState({hours: 0, minutes: 1, seconds: 0});

    const addWidget = () => {
        const timer = state.hours + state.minutes + state.seconds

        if (state.link && timer > 0) {
            let tmp = BoardState.widgets;
            const id = uuid();
            tmp.push({id: id, type: <Video  infos={{...state, id: id}}/>, grid: {x: 0, y: 0, w: 4, h: 2, minW: 4, minH: 2}})

            BoardSetState({ ...BoardState, widgets: tmp});
            closeDialog()
        }
    };

    const closeDialog = () => {
        DrawerState['YouTube']['video'] = !DrawerState['YouTube']['video']

        DrawerSetState({...DrawerState})
    }

  return (
    <Grid container style={{height: '100%'}} direction="column" justify='space-between'>
        <DialogTitle id="form-dialog-title">YouTube Video</DialogTitle>
        <DialogContent>
            <DialogContentText>This Widget needs a link to a Video</DialogContentText>
            <TextField
                autoFocus
                margin="dense"
                id="link"
                label="Link"
                type="text"
                fullWidth
                onChange={ event => setState({...state, link: event.target.value}) }
            />
        </DialogContent>

        <GetTimer state={state} setState={setState} />

        <DialogActions>
            <Button onClick={closeDialog} style={{outline: 'none'}}>
                Cancel
            </Button>
            <Button onClick={addWidget} style={{outline: 'none'}}>
                Add
            </Button>
        </DialogActions>
    </Grid>
  );
}