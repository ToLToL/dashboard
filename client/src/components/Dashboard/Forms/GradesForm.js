import React from 'react';
import Grid from '@material-ui/core/Grid';

import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';

import GetTimer from './GetTimer'

import { DrawerContext, BoardContext } from '../Dashboard'

import Grades from '../Widgets/Grades';
import uuid from 'uuid'

export default function SimpleWeatherBuilder() {
    const [DrawerState, DrawerSetState] = React.useContext(DrawerContext);
    const [BoardState, BoardSetState] = React.useContext(BoardContext);
    const [state, setState] = React.useState({hours: 0, minutes: 1, seconds: 0});

    const addWidget = () => {
        const timer = state.hours + state.minutes + state.seconds

        if (state.login && timer > 0) {
            let tmp = BoardState.widgets;
            const id = uuid();
            tmp.push({id: id, type: <Grades  infos={{...state, id: id}}/>, grid: {x: 0, y: 0, w: 4, h: 2, minW: 4, minH: 2}})

            BoardSetState({ ...BoardState, widgets: tmp});
            closeDialog()
        }
    };

    const closeDialog = () => {
        DrawerState['Epitech']['grades'] = !DrawerState['Epitech']['grades']

        DrawerSetState({...DrawerState})
    }

  return (
    <Grid container style={{height: '100%'}} direction="column" justify='space-between'>
        <DialogTitle id="form-dialog-title">Epitech User's Grades</DialogTitle>
        <DialogContent>
            <DialogContentText>This Widget needs a login</DialogContentText>
            <TextField
                autoFocus
                margin="dense"
                id="city"
                label="Login"
                type="text"
                fullWidth
                onChange={ event => setState({...state, login: event.target.value}) }
            />
        </DialogContent>

        <GetTimer state={state} setState={setState} />

        <DialogActions>
            <Button onClick={closeDialog} style={{outline: 'none'}}>
                Cancel
            </Button>
            <Button onClick={addWidget} style={{outline: 'none'}}>
                Add
            </Button>
        </DialogActions>
    </Grid>
  );
}