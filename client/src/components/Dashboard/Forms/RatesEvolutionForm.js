import React from 'react';
import Grid from '@material-ui/core/Grid';

import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';

import GetTimer from './GetTimer'

import InputLabel from '@material-ui/core/InputLabel';
import FormControl from '@material-ui/core/FormControl';
import MenuItem from '@material-ui/core/MenuItem';
import Select from '@material-ui/core/Select'

import { DrawerContext, BoardContext } from '../Dashboard'

import RatesEvolution from '../Widgets/RatesEvolution';
import uuid from 'uuid'

export default function RatesEvolutionBuilder() {
    const [DrawerState, DrawerSetState] = React.useContext(DrawerContext);
    const [BoardState, BoardSetState] = React.useContext(BoardContext);
    const [state, setState] = React.useState({hours: 0, minutes: 1, seconds: 0, frequency: 'daily'});

    const addWidget = () => {
        const timer = state.hours + state.minutes + state.seconds

        if (state.currency1 && state.currency2 && timer > 0) {
            let tmp = BoardState.widgets;
            const id = uuid();
            tmp.push({id: id, type: <RatesEvolution infos={{...state, id: id}}/>, grid: {x: 0, y: 0, w: 6, h: 4, minW: 6, minH: 4}})

            BoardSetState({ ...BoardState, widgets: tmp});
            closeDialog()
        }
    };

    const closeDialog = () => {
        DrawerState['Stock Exchanges']['rates evolution'] = !DrawerState['Stock Exchanges']['rates evolution']

        DrawerSetState({...DrawerState})
    }

  return (
    <Grid container style={{height: '100%'}} direction="column" justify='space-between'>
        <DialogTitle id="form-dialog-title">Rates Evolution Graph</DialogTitle>
        <DialogContent>
            <DialogContentText>This Widget needs 2 currencies</DialogContentText>
            <TextField
                autoFocus
                margin="dense"
                id="Currency1"
                label="Currency"
                type="text"
                fullWidth
                onChange={ event => setState({...state, currency1: event.target.value}) }
            />
        </DialogContent>

        <DialogContent>
            <TextField
                margin="dense"
                id="Currency2"
                label="Currency"
                type="text"
                fullWidth
                onChange={ event => setState({...state, currency2: event.target.value}) }
            />
        </DialogContent>

        <DialogContent>
            <FormControl>
                <InputLabel>Frequency</InputLabel>
                <Select labelId="frequency" id='frequency' value={state.frequency} onChange={event => setState({...state, frequency: event.target.value})}>
                    <MenuItem key={'daily'} value={'daily'}>Daily</MenuItem>
                    <MenuItem key={'weekly'} value={'weekly'}>Weekly</MenuItem>
                    <MenuItem key={'monthly'} value={'monthly'}>Monthly</MenuItem>
                </Select>
            </FormControl>
        </DialogContent>

        <GetTimer state={state} setState={setState} />

        <DialogActions>
            <Button onClick={closeDialog} style={{outline: 'none'}}>
                Cancel
            </Button>
            <Button onClick={addWidget} style={{outline: 'none'}}>
                Add
            </Button>
        </DialogActions>
    </Grid>
  );
}