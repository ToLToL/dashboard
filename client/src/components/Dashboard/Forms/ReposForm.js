import React from 'react';
import Grid from '@material-ui/core/Grid';

import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';

import IconButton from '@material-ui/core/IconButton';
import Visibility from '@material-ui/icons/Visibility';
import VisibilityOff from '@material-ui/icons/VisibilityOff';

import GetTimer from './GetTimer'

import { DrawerContext, BoardContext } from '../Dashboard'

import Repos from '../Widgets/Repos';
import uuid from 'uuid'

export default function ReposBuilder() {
    const [DrawerState, DrawerSetState] = React.useContext(DrawerContext);
    const [BoardState, BoardSetState] = React.useContext(BoardContext);
    const [state, setState] = React.useState({hours: 0, minutes: 1, seconds: 0, hidePassword: true});

    const addWidget = () => {
        const timer = state.hours + state.minutes + state.seconds

        if (state.username && state.password && timer > 0) {
            let tmp = BoardState.widgets;
            const id = uuid();
            tmp.push({id: id, type: <Repos  infos={{...state, id: id}}/>, grid: {x: 0, y: 0, w: 5, h: 2, minW: 5, minH: 2}})

            BoardSetState({ ...BoardState, widgets: tmp});
            closeDialog()
        }
    };

    const closeDialog = () => {
        DrawerState['GitHub']['repos'] = !DrawerState['GitHub']['repos']

        DrawerSetState({...DrawerState})
    }

  return (
    <Grid container style={{height: '100%'}} direction="column" justify='space-between'>
        <DialogTitle id="form-dialog-title">GitHub Repository list</DialogTitle>
        <DialogContent>
            <DialogContentText>This Widget needs your username and password</DialogContentText>
            <TextField
                autoFocus
                margin="dense"
                id="username"
                label="Username"
                type="text"
                fullWidth
                onChange={ event => setState({...state, username: event.target.value}) }
            />
        </DialogContent>

        <DialogContent>
            <TextField
                margin="dense"
                id="password"
                label="Password"
                type={state.hidePassword ? 'password' : 'text'}
                style={{width: '85%'}}
                onChange={ event => setState({...state, password: event.target.value}) }
            />
            <IconButton style={{outline: 'none', marginTop: '12px'}} onClick={() => setState({...state, hidePassword: !state.hidePassword}) }>
                {state.hidePassword ? <VisibilityOff /> : <Visibility />}
            </IconButton>
        </DialogContent>

        <GetTimer state={state} setState={setState} />

        <DialogActions>
            <Button onClick={closeDialog} style={{outline: 'none'}}>
                Cancel
            </Button>
            <Button onClick={addWidget} style={{outline: 'none'}}>
                Add
            </Button>
        </DialogActions>
    </Grid>
  );
}