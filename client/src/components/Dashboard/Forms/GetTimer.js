import React from 'react';

import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';

export default function GetTimer(props) {

  return (
        <div style={{display: 'flex', justifyContent: 'space-around'}}>
            <div style={{fontSize: 15, color: 'grey'}} className='py-3'>Refresh</div>
            <FormControl>
                <InputLabel id="hours">Hours</InputLabel>
                <Select
                labelId="hours"
                id="hours"
                value={props.state.hours}
                onChange={ event => props.setState({...props.state, hours: event.target.value}) }
                >
                    {[...Array(24).keys()].map((nb) => {return (<MenuItem key={nb} value={nb}>{nb}</MenuItem>)})}
                </Select>
            </FormControl>

            <FormControl>
                <InputLabel id="minutes">minutes</InputLabel>
                <Select
                labelId="minutes"
                id="minutes"
                value={props.state.minutes}
                onChange={ event => props.setState({...props.state, minutes: event.target.value}) }
                >
                    {[...Array(60).keys()].map((nb) => {return (<MenuItem key={nb} value={nb}>{nb}</MenuItem>)})}
                </Select>
            </FormControl>

            <FormControl>
                <InputLabel id="seconds">Seconds</InputLabel>
                <Select
                labelId="seconds"
                id="seconds"
                value={props.state.seconds}
                onChange={ event => props.setState({...props.state, seconds: event.target.value}) }
                >
                    {[...Array(60).keys()].map((nb) => {return (<MenuItem key={nb} value={nb}>{nb}</MenuItem>)})}
                </Select>
            </FormControl>
        </div>
  );
}