import React from 'react';
import Drawer from '@material-ui/core/Drawer';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import Collapse from '@material-ui/core/Collapse';

import Dialog from '@material-ui/core/Dialog';

import ExpandLess from '@material-ui/icons/ExpandLess';
import ExpandMore from '@material-ui/icons/ExpandMore';

import { DrawerContext } from './Dashboard'
import { WidgetList } from './WidgetList'

export default function TemporaryDrawer() {
    const [state, setState] = React.useContext(DrawerContext);

    const toggleDrawer = () => {
        setState({ ...state, openDrawer: !state.openDrawer});
    };

    const handleClickService = service => {
        state[service].isOpen = !state[service].isOpen;

        setState({...state})
    }

    const handleClickWidget = (service, widget) => {
        state[service][widget] = !state[service][widget]

        setState({...state})
    }

    return (
        <Drawer open={state.openDrawer} onClose={toggleDrawer}>
            <List style={{minWidth: '10vw', maxWidth: '20vw'}}>
                {WidgetList.map(({service, serviceIcon, widgets}) => (
                    <div key={service}>
                        <ListItem button onClick={() => handleClickService(service)}>
                            <ListItemIcon>{serviceIcon}</ListItemIcon>
                            <ListItemText className='mr-3' primary={service}/>
                            {state[service].isOpen ? <ExpandLess /> : <ExpandMore />}
                        </ListItem>

                        <Collapse in={state[service].isOpen} timeout="auto" unmountOnExit>
                            <List>
                                {widgets.map(({name, icon, type, grid}) => (
                                    <div key={name}>
                                        <ListItem button onClick={() => handleClickWidget(service, name)}>
                                            <ListItemIcon className='pl-2'>{icon}</ListItemIcon>
                                            <ListItemText primary={name} />
                                        </ListItem>

                                        <Dialog open={state[service][name]} onClose={() => handleClickWidget(service, name)}>
                                            {type}
                                        </Dialog>
                                    </div>
                                ))}
                            </List>
                        </Collapse>
                    </div>
                ))}
            </List>
        </Drawer>
    );
}