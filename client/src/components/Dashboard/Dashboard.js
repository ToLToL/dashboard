import React, { Component, useState } from 'react';
import Header from './Header';
import TemporaryDrawer from './TemporaryDrawer';
import Board from './Board';

export const DrawerContext = React.createContext([{}, () => {}]);
export const DrawerProvider = props => {
    const [state, setState] = useState({
        openDrawer: false,
        'Weather': {
            isOpen :false,
            'simple' : false,
            'complex' : false,
            'forecast' : false,
        },
        'Stock Exchanges': {
            isOpen :false,
            'rates' : false,
            'rates evolution' : false,
            'equity evolution' : false,
        },
        'Epitech': {
            isOpen: false,
            'user': false,
            'groups': false,
            'grades': false,
        },
        'GitHub': {
            isOpen: false,
            'repos': false,
        },
        'Crypto-Trading': {
            isOpen: false,
            'cryptoRates': false,
        },
        'YouTube': {
            isOpen: false,
            'channel': false,
            'video': false,
        },
        'Definition': {
            isOpen: false,
        },
    });

    return (
        <DrawerContext.Provider value={[state, setState]}>
            {props.children}
        </DrawerContext.Provider>
    );
}

export const BoardContext = React.createContext([{}, () => {}]);
export const BoardProvider = props => {
    const [state, setState] = useState({widgets: []});

    return (
        <BoardContext.Provider value={[state, setState]}>
            {props.children}
        </BoardContext.Provider>
    );
}

class Dashboard extends Component {
    render() {
        return (
            <DrawerProvider>
                <Header />
                <BoardProvider>
                    <TemporaryDrawer />
                    <Board />
                </BoardProvider>
            </DrawerProvider>
        );
    }
};

export default Dashboard;