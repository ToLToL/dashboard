import React from 'react';
import { Responsive, WidthProvider } from 'react-grid-layout';
import Card from '@material-ui/core/Card';

import { BoardContext } from './Dashboard';

const ResponsiveGridLayout = WidthProvider(Responsive);

export default function Board() {
    const [state] = React.useContext(BoardContext);

    return (
        <ResponsiveGridLayout className="layout">
            {state.widgets.map(({id, type, grid}) => (
                <Card key={id} data-grid={grid} style={{borderWidth: '1px', borderStyle: 'solid', borderColor: 'grey'}}>
                    {type}
                </Card>
            ))}
        </ResponsiveGridLayout>
    )
}