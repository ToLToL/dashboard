import React from 'react';

import SunIcon from '@material-ui/icons/WbSunny';
import SimpleIcon from '@material-ui/icons/ScatterPlot';
import ComplexeIcon from '@material-ui/icons/AcUnit';
import FutureIcon from '@material-ui/icons/Update';

import StockIcon from '@material-ui/icons/EuroSymbol';
import RatesIcon from '@material-ui/icons/CompareArrows';
import EquityIcon from '@material-ui/icons/Timeline';

import EpitechIcon from '@material-ui/icons/Explicit';
import UserIcon from '@material-ui/icons/EmojiPeople';
import GroupIcon from '@material-ui/icons/People';
import GradesIcon from '@material-ui/icons/Book';

import GitHubIcon from '@material-ui/icons/GitHub';
import RepoIcon from '@material-ui/icons/AllInbox';

import CryptoIcon from '@material-ui/icons/AccountBalanceWallet';

import YouTubeIcon from '@material-ui/icons/YouTube';
import ChannelIcon from '@material-ui/icons/Subscriptions';
import VideoIcon from '@material-ui/icons/Videocam';

import DefinitionIcon from '@material-ui/icons/LocalLibrary';


import SimpleWeatherForm from './Forms/SimpleWeatherForm';
import ComplexWeatherForm from './Forms/ComplexWeatherForm';
import ForecastForm from './Forms/ForecastForm';

import RatesForm from './Forms/RatesForm';
import RatesEvolutionForm from './Forms/RatesEvolutionForm';
import EquityEvolutionForm from './Forms/EquityEvolutionForm';

import UserForm from './Forms/UserForm';
import GroupForm from './Forms/GroupForm';
import GradesForm from './Forms/GradesForm';

import ReposForm from './Forms/ReposForm';

import CryptoRatesForm from './Forms/CryptoRatesForm';

import ChannelForm from './Forms/ChannelForm';
import VideoForm from './Forms/VideoForm';

import DefinitionForm from './Forms/DefinitionForm';


export const WidgetList = [
    {service: 'Weather', serviceIcon: <SunIcon />,
        widgets: [
            {name: 'simple', icon: <SimpleIcon />, type: <SimpleWeatherForm/>},
            {name: 'complex', icon: <ComplexeIcon />, type: <ComplexWeatherForm/>},
            {name: 'forecast', icon: <FutureIcon />, type: <ForecastForm/>},
        ]
    },
    {service: 'Stock Exchanges', serviceIcon: <StockIcon />,
        widgets: [
            {name: 'rates', icon: <RatesIcon />, type: <RatesForm/>},
            {name: 'rates evolution', icon: <EquityIcon />, type: <RatesEvolutionForm/>},
            {name: 'equity evolution', icon: <EquityIcon />, type: <EquityEvolutionForm/>},
        ]
    },
    {service: 'Epitech', serviceIcon: <EpitechIcon />,
        widgets: [
            {name: 'user', icon: <UserIcon />, type: <UserForm/>},
            {name: 'groups', icon: <GroupIcon />, type: <GroupForm/>},
            {name: 'grades', icon: <GradesIcon />, type: <GradesForm/>},
        ]
    },
    {service: 'GitHub', serviceIcon: <GitHubIcon />,
        widgets: [
            {name: 'repos', icon: <RepoIcon />, type: <ReposForm/>},
        ]
    },
    {service: 'Crypto-Trading', serviceIcon: <CryptoIcon />,
        widgets: [
            {name: 'cryptoRates', icon: <RatesIcon />, type: <CryptoRatesForm/>},
        ]
    },
    {service: 'YouTube', serviceIcon: <YouTubeIcon />,
        widgets: [
            {name: 'channel', icon: <ChannelIcon />, type: <ChannelForm/>},
            {name: 'video', icon: <VideoIcon />, type: <VideoForm/>},
        ]
    },
    {service: 'Definition', serviceIcon: <DefinitionIcon />,
        widgets: [
            {name: 'definition', icon: <DefinitionIcon />, type: <DefinitionForm/>},
        ]
    },
].sort((a, b) => a.service > b.service);
