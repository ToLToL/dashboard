import React, { useState, useEffect } from 'react';
import axios from 'axios';

import Toolbar from '@material-ui/core/Toolbar';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import AppBar from '@material-ui/core/AppBar';

import Dialog from '@material-ui/core/Dialog';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';

import IconButton from '@material-ui/core/IconButton';
import RefreshIcon from '@material-ui/icons/Refresh';
import SettingsIcon from '@material-ui/icons/Settings';

import GetTimer from '../Forms/GetTimer'

import { BoardContext } from '../Dashboard';

export default function Channel(props) {
    const [params, setParams] = useState({ openSettings: false, ...props.infos, interval: null });
    const [infos, setInfos] = useState({});

    const updateInfos = (url) => {
        axios.get(`http://localhost:8080/api/youtube/channel?url=${url}`)
        .then(({data}) => {console.log(data); setInfos({...data})})
    };

    useEffect(() => {
        updateInfos(params.link)

        params.interval = setInterval(() => updateInfos(params.link), 1000 * (params.hours * 3600 + params.minutes * 60 + params.seconds))
        // eslint-disable-next-line
    }, []);

    const UpdateForm = () => {
        const [boardState, setBoardState] = React.useContext(BoardContext);
        const [formState, setFormState] = useState({...params})

        const onConfirm = () => {
            formState.openSettings = false
            clearInterval(params.interval)
            setParams({
                ...formState,
                openSettings: false,
                interval: setInterval(() => updateInfos(formState.link), 1000 * (formState.hours * 3600 + formState.minutes * 60 + formState.seconds)),
            })
            updateInfos(formState.link)
        }

        const onCancel = () => {
            setFormState({...params})
            setParams({...params, openSettings: false})
        }

        const onDelete = () => {
            clearInterval(params.interval)
            const widgets = boardState.widgets.filter(({id}) => {return id !== params.id})
            setBoardState({
                widgets
            })
            setParams({...params, openSettings: false})
        }

        return (
            <Dialog open={params.openSettings}  onClose={onCancel}>
                <DialogTitle id="form-dialog-title">YouTube Channel</DialogTitle>
                <DialogContent>
                    <DialogContentText>This Widget needs a link to a Channel</DialogContentText>
                    <TextField
                        autoFocus
                        margin="dense"
                        id="link"
                        label="Link"
                        type="text"
                        fullWidth
                        value={formState.link}
                        onChange={event => setFormState({...formState, link: event.target.value})}
                    />
                </DialogContent>

                <GetTimer state={formState} setState={setFormState} />

                <DialogActions>
                    <Button onClick={onConfirm} style={{outline: 'none'}}>
                        Confirm
                    </Button>
                    <Button onClick={onCancel} style={{outline: 'none'}}>
                        Cancel
                    </Button>
                    <Button onClick={onDelete} style={{outline: 'none', color: 'red'}}>
                        Delete
                    </Button>
                </DialogActions>
            </Dialog>
        )
    }

    return (
        <Grid container style={{height: '100%'}} direction="column">
            <Grid item>
                <AppBar>
                    <Toolbar style={{justifyContent: 'space-between'}}>
                        <IconButton style={{outline: 'none'}} onClick={() => updateInfos(params.link)}><RefreshIcon /></IconButton>
                        <Typography variant='h6'>YouTube Channel</Typography>
                        <IconButton style={{outline: 'none'}} onClick={() => {setParams({ ...params, openSettings: true})}}><SettingsIcon /></IconButton>
                    </Toolbar>
                </AppBar>
            </Grid>

            <Grid item container style={{height: '100%', paddingTop: '64px'}} direction="column" justify='space-around'>
                <Grid item>
                    <h5 style={{textAlign: 'center'}}>{infos.title}</h5>
                </Grid>

                <Grid item>
                    <Toolbar style={{justifyContent: 'space-around'}}>
                        <img style={{maxWidth: '15%', maxHeight: '15%'}} src={infos.imageUrl} alt='Icon'/>
                        <div>{'subs: ' + infos.subscriberCount}</div>
                        <div>{'videos: ' + infos.videoCount}</div>
                    </Toolbar>
                </Grid>

                <Grid item>
                    <p style={{textAlign: 'center'}}>{infos.description}</p>
                </Grid>
            </Grid>

            <UpdateForm/>
        </Grid>
    )
}