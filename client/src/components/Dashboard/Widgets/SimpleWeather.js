import React, { useState, useEffect } from 'react';
import axios from 'axios';

import Toolbar from '@material-ui/core/Toolbar';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import AppBar from '@material-ui/core/AppBar';

import Dialog from '@material-ui/core/Dialog';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';

import IconButton from '@material-ui/core/IconButton';
import RefreshIcon from '@material-ui/icons/Refresh';
import SettingsIcon from '@material-ui/icons/Settings';

import GetTimer from '../Forms/GetTimer'

import { BoardContext } from '../Dashboard';

export default function SimpleWeather(props) {
    const [params, setParams] = useState({ openSettings: false, ...props.infos, interval: null });
    const [infos, setInfos] = useState({});

    const updateInfos = (city) => {
        axios.get(`http://localhost:8080/api/weather/simple?city=${city}`)
        .then(({data}) => {console.log(data); setInfos({...data})})
    };

    useEffect(() => {
        updateInfos(params.city)

        params.interval = setInterval(() => updateInfos(params.city), 1000 * (params.hours * 3600 + params.minutes * 60 + params.seconds))
        // eslint-disable-next-line
    }, []);

    const UpdateForm = () => {
        const [boardState, setBoardState] = React.useContext(BoardContext);
        const [formState, setFormState] = useState({...params})

        const onConfirm = () => {
            formState.openSettings = false
            clearInterval(params.interval)
            setParams({
                ...formState,
                openSettings: false,
                interval: setInterval(() => updateInfos(formState.city), 1000 * (formState.hours * 3600 + formState.minutes * 60 + formState.seconds)),
            })
            updateInfos(formState.city)
        }

        const onCancel = () => {
            setFormState({...params})
            setParams({...params, openSettings: false})
        }

        const onDelete = () => {
            clearInterval(params.interval)
            const widgets = boardState.widgets.filter(({id}) => {return id !== params.id})
            setBoardState({
                widgets
            })
            setParams({...params, openSettings: false})
        }

        return (
            <Dialog open={params.openSettings}  onClose={onCancel}>
                <DialogTitle id="form-dialog-title">Simple Weather</DialogTitle>
                <DialogContent>
                    <DialogContentText>This Widget needs the name of a city</DialogContentText>
                    <TextField
                        autoFocus
                        margin="dense"
                        id="city"
                        label="City"
                        type="text"
                        fullWidth
                        value={formState.city}
                        onChange={event => setFormState({...formState, city: event.target.value})}
                    />
                </DialogContent>

                <GetTimer state={formState} setState={setFormState} />

                <DialogActions>
                    <Button onClick={onConfirm} style={{outline: 'none'}}>
                        Confirm
                    </Button>
                    <Button onClick={onCancel} style={{outline: 'none'}}>
                        Cancel
                    </Button>
                    <Button onClick={onDelete} style={{outline: 'none', color: 'red'}}>
                        Delete
                    </Button>
                </DialogActions>
            </Dialog>
        )
    }

    return (
        <Grid container style={{height: '100%'}} direction="column" justify='space-between'>
            <Grid item>
                <AppBar>
                    <Toolbar style={{justifyContent: 'space-between'}}>
                        <IconButton style={{outline: 'none'}} onClick={() => updateInfos(params.city)}><RefreshIcon /></IconButton>
                        <Typography variant='h6'>Simple Weather</Typography>
                        <IconButton style={{outline: 'none'}} onClick={() => {setParams({ ...params, openSettings: true})}}><SettingsIcon /></IconButton>
                    </Toolbar>
                </AppBar>
            </Grid>

            <Grid item container style={{height: '70%'}} direction="column" justify='space-around'>
                <Grid item>
                    <h5 style={{textAlign: 'center'}}>{infos.city}</h5>
                </Grid>

                <Grid item>
                    <Toolbar style={{justifyContent: 'space-around'}}>
                        <img src={infos.icon} alt='Icon'/>
                        <div>{infos.temperature}</div>
                    </Toolbar>
                </Grid>
            </Grid>

            <UpdateForm/>
        </Grid>
    )
}