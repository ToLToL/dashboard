import React, { useState, useEffect } from 'react';
import axios from 'axios';

import Toolbar from '@material-ui/core/Toolbar';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import AppBar from '@material-ui/core/AppBar';

import Dialog from '@material-ui/core/Dialog';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';

import IconButton from '@material-ui/core/IconButton';
import RefreshIcon from '@material-ui/icons/Refresh';
import SettingsIcon from '@material-ui/icons/Settings';

import GetTimer from '../Forms/GetTimer'

import { BoardContext } from '../Dashboard';

export default function Rates(props) {
    const [params, setParams] = useState({ openSettings: false, ...props.infos, interval: null });
    const [infos, setInfos] = useState({});

    const updateInfos = (currency1, currency2) => {
        axios.get(`http://localhost:8080/api/crypto?from_crypto=${currency1}&to_currency=${currency2}`)
        .then(({data}) => {console.log(data); setInfos({...data})})
    };

    useEffect(() => {
        updateInfos(params.currency1, params.currency2)

        params.interval = setInterval(() => updateInfos(params.currency1, params.currency2), 1000 * (params.hours * 3600 + params.minutes * 60 + params.seconds))
        // eslint-disable-next-line
    }, []);

    const UpdateForm = () => {
        const [boardState, setBoardState] = React.useContext(BoardContext);
        const [formState, setFormState] = useState({...params})

        const onConfirm = () => {
            formState.openSettings = false
            clearInterval(params.interval)
            setParams({
                ...formState,
                openSettings: false,
                interval: setInterval(() => updateInfos(formState.currency1, formState.currency2), 1000 * (formState.hours * 3600 + formState.minutes * 60 + formState.seconds)),
            })
            updateInfos(formState.currency1, formState.currency2)
        }

        const onCancel = () => {
            setFormState({...params})
            setParams({...params, openSettings: false})
        }

        const onDelete = () => {
            clearInterval(params.interval)
            const widgets = boardState.widgets.filter(({id}) => {return id !== params.id})
            setBoardState({
                widgets
            })
            setParams({...params, openSettings: false})
        }

        return (
            <Dialog open={params.openSettings}  onClose={onCancel}>
                <DialogTitle id="form-dialog-title">Rates</DialogTitle>
                <DialogContent>
                    <DialogContentText>This Widget needs 2 currencies</DialogContentText>
                    <TextField
                        autoFocus
                        margin="dense"
                        id="Currency1"
                        label="Currency"
                        type="text"
                        fullWidth
                        value={formState.currency1}
                        onChange={ event => setFormState({...formState, currency1: event.target.value}) }
                    />
                </DialogContent>

                <DialogContent>
                    <TextField
                        margin="dense"
                        id="Currency2"
                        label="Currency"
                        type="text"
                        fullWidth
                        value={formState.currency2}
                        onChange={ event => setFormState({...formState, currency2: event.target.value}) }
                    />
                </DialogContent>


                <GetTimer state={formState} setState={setFormState} />

                <DialogActions>
                    <Button onClick={onConfirm} style={{outline: 'none'}}>
                        Confirm
                    </Button>
                    <Button onClick={onCancel} style={{outline: 'none'}}>
                        Cancel
                    </Button>
                    <Button onClick={onDelete} style={{outline: 'none', color: 'red'}}>
                        Delete
                    </Button>
                </DialogActions>
            </Dialog>
        )
    }

    return (
        <Grid container style={{height: '100%'}} direction="column" justify='space-between'>
            <Grid item>
                <AppBar>
                    <Toolbar style={{justifyContent: 'space-between'}}>
                        <IconButton style={{outline: 'none'}} onClick={() => updateInfos(params.currency1, params.currency2)}><RefreshIcon /></IconButton>
                        <Typography variant='h6'>CryptoRates</Typography>
                        <IconButton style={{outline: 'none'}} onClick={() => {setParams({ ...params, openSettings: true})}}><SettingsIcon /></IconButton>
                    </Toolbar>
                </AppBar>
            </Grid>

            <Grid item style={{height: '70%'}} className='pt-4'>
                    <Toolbar style={{justifyContent: 'space-around'}}>
                        <h5>{params.currency1}</h5>
                        <h5> = </h5>
                        <h5>{infos.exchangeRate}</h5>
                        <h5> * </h5>
                        <h5>{params.currency2}</h5>
                    </Toolbar>
            </Grid>

            <UpdateForm/>
        </Grid>
    )
}