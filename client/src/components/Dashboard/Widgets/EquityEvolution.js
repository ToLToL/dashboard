import React, { useState, useEffect } from 'react';
import axios from 'axios';

import Toolbar from '@material-ui/core/Toolbar';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import AppBar from '@material-ui/core/AppBar';

import Dialog from '@material-ui/core/Dialog';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';

import InputLabel from '@material-ui/core/InputLabel';
import FormControl from '@material-ui/core/FormControl';
import MenuItem from '@material-ui/core/MenuItem';
import Select from '@material-ui/core/Select'

import IconButton from '@material-ui/core/IconButton';
import RefreshIcon from '@material-ui/icons/Refresh';
import SettingsIcon from '@material-ui/icons/Settings';

import GetTimer from '../Forms/GetTimer'

import { Line } from 'react-chartjs-2';

import { BoardContext } from '../Dashboard';

export default function EquityEvolution(props) {
    const [params, setParams] = useState({ openSettings: false, ...props.infos, interval: null });
    const [infos, setInfos] = useState({evolution: {}});

    const updateInfos = (currency, frequency) => {
        axios.get(`http://localhost:8080/api/stock/equity/${frequency}?equity=${currency}`)
        .then(({data}) => {console.log(data); setInfos({...data})})
    };

    useEffect(() => {
        updateInfos(params.currency, params.frequency)

        params.interval = setInterval(() => updateInfos(params.currency, params.frequency), 1000 * (params.hours * 3600 + params.minutes * 60 + params.seconds))
        // eslint-disable-next-line
    }, []);

    const UpdateForm = () => {
        const [boardState, setBoardState] = React.useContext(BoardContext);
        const [formState, setFormState] = useState({...params})

        const onConfirm = () => {
            formState.openSettings = false
            clearInterval(params.interval)
            setParams({
                ...formState,
                openSettings: false,
                interval: setInterval(() => updateInfos(formState.currency, formState.frequency), 1000 * (formState.hours * 3600 + formState.minutes * 60 + formState.seconds)),
            })
            updateInfos(formState.currency, formState.frequency)
        }

        const onCancel = () => {
            setFormState({...params})
            setParams({...params, openSettings: false})
        }

        const onDelete = () => {
            clearInterval(params.interval)
            const widgets = boardState.widgets.filter(({id}) => {return id !== params.id})
            setBoardState({
                widgets
            })
            setParams({...params, openSettings: false})
        }

        return (
            <Dialog open={params.openSettings}  onClose={onCancel}>
                <DialogTitle id="form-dialog-title">Equity Evolution Graph</DialogTitle>
                <DialogContent>
                    <DialogContentText>This Widget needs a currency</DialogContentText>
                    <TextField
                        autoFocus
                        margin="dense"
                        id="Currency"
                        label="Currency"
                        type="text"
                        fullWidth
                        value={formState.currency}
                        onChange={ event => setFormState({...formState, currency: event.target.value}) }
                    />
                </DialogContent>

                <DialogContent>
                    <FormControl>
                        <InputLabel>Frequency</InputLabel>
                        <Select labelId="frequency" id='frequency' value={formState.frequency} onChange={event => setFormState({...formState, frequency: event.target.value})}>
                            <MenuItem key={'daily'} value={'daily'}>Daily</MenuItem>
                            <MenuItem key={'weekly'} value={'weekly'}>Weekly</MenuItem>
                            <MenuItem key={'monthly'} value={'monthly'}>Monthly</MenuItem>
                        </Select>
                    </FormControl>
                </DialogContent>

                <GetTimer state={formState} setState={setFormState} />

                <DialogActions>
                    <Button onClick={onConfirm} style={{outline: 'none'}}>
                        Confirm
                    </Button>
                    <Button onClick={onCancel} style={{outline: 'none'}}>
                        Cancel
                    </Button>
                    <Button onClick={onDelete} style={{outline: 'none', color: 'red'}}>
                        Delete
                    </Button>
                </DialogActions>
            </Dialog>
        )
    }

    return (
        <div>
            <Grid container style={{height: '100%'}} direction="column" justify='space-between'>
                <Grid item>
                    <AppBar>
                        <Toolbar style={{justifyContent: 'space-between'}}>
                            <IconButton style={{outline: 'none'}} onClick={() => updateInfos(params.currency, params.frequency)}><RefreshIcon /></IconButton>
                            <Typography variant='h6'>{`${infos.equity} Equity's ${params.frequency} evolution`}</Typography>
                            <IconButton style={{outline: 'none'}} onClick={() => {setParams({ ...params, openSettings: true})}}><SettingsIcon /></IconButton>
                        </Toolbar>
                    </AppBar>
                </Grid>

                <Grid item container style={{paddingTop: '70px'}}>
                        <Line legend={{display: false}} data={{
                            labels: Object.keys(infos.evolution),
                            datasets: [
                                {
                                    data: Object.keys(infos.evolution).map(date => { return infos.evolution[date]['1. open'] }),
                                },
                            ]
                        }
                        }/>
                </Grid>
            </Grid>
            <UpdateForm/>
        </div>
    )
}