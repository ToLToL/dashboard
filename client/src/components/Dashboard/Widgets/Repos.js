import React, { useState, useEffect } from 'react';
import axios from 'axios';

import Toolbar from '@material-ui/core/Toolbar';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import AppBar from '@material-ui/core/AppBar';

import Paper from '@material-ui/core/Paper';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';

import Dialog from '@material-ui/core/Dialog';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';

import IconButton from '@material-ui/core/IconButton';
import RefreshIcon from '@material-ui/icons/Refresh';
import SettingsIcon from '@material-ui/icons/Settings';

import Visibility from '@material-ui/icons/Visibility';
import VisibilityOff from '@material-ui/icons/VisibilityOff';

import GetTimer from '../Forms/GetTimer'

import { BoardContext } from '../Dashboard';

import uuid from 'uuid'

export default function Repos(props) {
    const [params, setParams] = useState({ openSettings: false, ...props.infos, interval: null });
    const [infos, setInfos] = useState({repos: []});

    const updateInfos = (username, password) => {
        console.log('Get REPOS')
        axios.get('http://localhost:8080/api/github/repos')
        .then(({data}) => {
            console.log(data)
            setInfos({repos: data})
        })
    };

    useEffect(() => {
        console.log(`Connect`)
        axios.get(`http://localhost:8080/api/github/login?username=${params.username}&password=${params.password}`)
        .then(({data}) => {
            console.log(data)
            updateInfos(params.username, params.password)
        })


        params.interval = setInterval(() => updateInfos(params.username, params.password), 1000 * (params.hours * 3600 + params.minutes * 60 + params.seconds))
        // eslint-disable-next-line
    }, []);

    const UpdateForm = () => {
        const [boardState, setBoardState] = React.useContext(BoardContext);
        const [formState, setFormState] = useState({...params, hidePassword: true, private: false, autoInit: false})

        const onConfirm = () => {
            formState.openSettings = false
            axios.get(`http://localhost:8080/api/github/login?username=${formState.username}&password=${formState.password}`)
            clearInterval(params.interval)
            setParams({
                ...formState,
                openSettings: false,
                interval: setInterval(() => updateInfos(formState.username, formState.password), 1000 * (formState.hours * 3600 + formState.minutes * 60 + formState.seconds)),
            })
            updateInfos(formState.username, formState.password)
        }

        const onCreate = () => {
            axios.get(`http://localhost:8080/api/github/create/repo?name=${formState.repoName}&description=${formState.repoDescription}&private=${formState.private}&auto_init=${formState.autoInit}`)
            onCancel()
        }

        const onCancel = () => {
            setFormState({...params})
            setParams({...params, openSettings: false})
        }

        const onDelete = () => {
            clearInterval(params.interval)
            const widgets = boardState.widgets.filter(({id}) => {return id !== params.id})
            setBoardState({
                widgets
            })
            setParams({...params, openSettings: false})
        }

        return (
            <Dialog open={params.openSettings}  onClose={onCancel}>
                <DialogTitle id="form-dialog-title">Add Repository</DialogTitle>
                <DialogContent>
                    <DialogContentText>Please fill the following informations</DialogContentText>
                    <TextField
                        autoFocus
                        margin="dense"
                        id="repoName"
                        label="Name"
                        type="text"
                        fullWidth
                        onChange={event => setFormState({...formState, repoName: event.target.value})}
                    />
                </DialogContent>

                <DialogContent>
                    <TextField
                        autoFocus
                        margin="dense"
                        id="repoDesription"
                        label="Description"
                        type="text"
                        fullWidth
                        onChange={event => setFormState({...formState, repoDescription: event.target.value})}
                    />
                </DialogContent>

                <DialogContent>
                    <FormControlLabel label="Private" control={
                            <Checkbox
                                checked={formState.private}
                                onChange={() => setFormState({...formState, private: !formState.private})}
                                color="primary"
                            />
                        }
                    />
                    <FormControlLabel label="Auto-Init" control={
                            <Checkbox
                                checked={formState.autoInit}
                                onChange={() => setFormState({...formState, autoInit: !formState.autoInit})}
                                color="primary"
                            />
                        }
                    />
                </DialogContent>

                <DialogActions>
                    <Button onClick={onCreate} style={{outline: 'none'}}>
                        Create Repository
                    </Button>
                </DialogActions>

                <DialogContent>
                    <DialogContentText>Change connected user</DialogContentText>
                    <TextField
                        autoFocus
                        margin="dense"
                        id="username"
                        label="Username"
                        type="text"
                        fullWidth
                        value={formState.username}
                        onChange={event => setFormState({...formState, username: event.target.value})}
                    />
                </DialogContent>

                <DialogContent>
                    <TextField
                        margin="dense"
                        id="password"
                        label="Password"
                        type={formState.hidePassword ? 'password' : 'text'}
                        style={{width: '80%'}}
                        value={formState.password}
                        onChange={ event => setFormState({...formState, password: event.target.value}) }
                    />
                    <IconButton style={{outline: 'none', marginTop: '12px'}} onClick={() => setFormState({...formState, hidePassword: !formState.hidePassword}) }>
                        {formState.hidePassword ? <VisibilityOff /> : <Visibility />}
                    </IconButton>
                </DialogContent>

                <GetTimer state={formState} setState={setFormState} />

                <DialogActions>
                    <Button onClick={onConfirm} style={{outline: 'none'}}>
                        Confirm
                    </Button>
                    <Button onClick={onCancel} style={{outline: 'none'}}>
                        Cancel
                    </Button>
                    <Button onClick={onDelete} style={{outline: 'none', color: 'red'}}>
                        Delete
                    </Button>
                </DialogActions>
            </Dialog>
        )
    }


    const StickyHeadTable = () => {
        return (
            <Paper style={{width: '100%'}}>
                <div style={{height: '50%', overflow: 'auto'}}>
                    <Table stickyHeader>
                    <TableHead>
                        <TableRow>
                            <TableCell key={'name'} align={'left'} style={{ minWidth: '30%' }}>{'name'}</TableCell>
                            <TableCell key={'url'} align={'center'} style={{ minWidth: '40%' }}>{'url'}</TableCell>
                            <TableCell key={'stars'} align={'center'} style={{ minWidth: '15%' }}>{'stars'}</TableCell>
                            <TableCell key={'forks'} align={'center'} style={{ minWidth: '15%' }}>{'forks'}</TableCell>
                        </TableRow>
                    </TableHead>

                    <TableBody>
                        {infos.repos.map(({name, url, stars, forks}) => {
                            return (
                                <TableRow hover role="checkbox" tabIndex={-1} key={uuid()}>
                                        <TableCell key={'name'} align={'left'}>{name}</TableCell>
                                        {/* eslint-disable-next-line */}
                                        <TableCell key={'url'} align={'center'}><a href={url} target='_blank'>{url}</a></TableCell>
                                        <TableCell key={'stars'} align={'center'}>{stars}</TableCell>
                                        <TableCell key={'forks'} align={'center'}>{forks}</TableCell>
                                </TableRow>
                            );
                        })}
                    </TableBody>
                    </Table>
                </div>
            </Paper>
        );
    }

    return (
        <div>
            <Grid container style={{height: '100%'}} direction="column" justify='space-between'>
                <Grid item>
                    <AppBar>
                        <Toolbar style={{justifyContent: 'space-between'}}>
                            <IconButton style={{outline: 'none'}} onClick={() => updateInfos(params.username, params.password)}><RefreshIcon /></IconButton>
                            <Typography variant='h6'>GitHub Repositories</Typography>
                            <IconButton style={{outline: 'none'}} onClick={() => {setParams({ ...params, openSettings: true})}}><SettingsIcon /></IconButton>
                        </Toolbar>
                    </AppBar>
                </Grid>

                <Grid item container style={{paddingTop: '64px'}}>
                    <StickyHeadTable />
                </Grid>
            </Grid>

            <UpdateForm/>
        </div>
    )
}