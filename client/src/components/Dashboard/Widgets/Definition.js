import React, { useState, useEffect } from 'react';
import axios from 'axios';

import Toolbar from '@material-ui/core/Toolbar';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import AppBar from '@material-ui/core/AppBar';

import Dialog from '@material-ui/core/Dialog';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';

import IconButton from '@material-ui/core/IconButton';
import RefreshIcon from '@material-ui/icons/Refresh';
import SettingsIcon from '@material-ui/icons/Settings';

import GetTimer from '../Forms/GetTimer'

import { BoardContext } from '../Dashboard';

export default function Definition(props) {
    const [params, setParams] = useState({ openSettings: false, ...props.infos, interval: null });
    const [infos, setInfos] = useState({});

    const updateInfos = (asked) => {
        axios.get(`http://localhost:8080/api/wikipedia/article?search=${asked}`)
        .then(({data}) => {console.log(data); setInfos({...data})})
    };

    useEffect(() => {
        updateInfos(params.asked)

        params.interval = setInterval(() => updateInfos(params.asked), 1000 * (params.hours * 3600 + params.minutes * 60 + params.seconds))
        // eslint-disable-next-line
    }, []);

    const UpdateForm = () => {
        const [boardState, setBoardState] = React.useContext(BoardContext);
        const [formState, setFormState] = useState({...params})

        const onConfirm = () => {
            formState.openSettings = false
            clearInterval(params.interval)
            setParams({
                ...formState,
                openSettings: false,
                interval: setInterval(() => updateInfos(formState.asked), 1000 * (formState.hours * 3600 + formState.minutes * 60 + formState.seconds)),
            })
            updateInfos(formState.asked)
        }

        const onCancel = () => {
            setFormState({...params})
            setParams({...params, openSettings: false})
        }

        const onDelete = () => {
            clearInterval(params.interval)
            const widgets = boardState.widgets.filter(({id}) => {return id !== params.id})
            setBoardState({
                widgets
            })
            setParams({...params, openSettings: false})
        }

        return (
            <Dialog open={params.openSettings}  onClose={onCancel}>
                <DialogTitle id="form-dialog-title">Definition</DialogTitle>
                <DialogContent>
                    <DialogContentText>This Widget needs a research</DialogContentText>
                    <TextField
                        autoFocus
                        margin="dense"
                        id="research"
                        label="Research"
                        type="text"
                        fullWidth
                        value={formState.asked}
                        onChange={event => setFormState({...formState, asked: event.target.value})}
                    />
                </DialogContent>

                <GetTimer state={formState} setState={setFormState} />

                <DialogActions>
                    <Button onClick={onConfirm} style={{outline: 'none'}}>
                        Confirm
                    </Button>
                    <Button onClick={onCancel} style={{outline: 'none'}}>
                        Cancel
                    </Button>
                    <Button onClick={onDelete} style={{outline: 'none', color: 'red'}}>
                        Delete
                    </Button>
                </DialogActions>
            </Dialog>
        )
    }

    return (
        <Grid container style={{height: '100%'}} direction="column">
            <Grid item>
                <AppBar>
                    <Toolbar style={{justifyContent: 'space-between'}}>
                        <IconButton style={{outline: 'none'}} onClick={() => updateInfos(params.asked)}><RefreshIcon /></IconButton>
                        <Typography variant='h6'>Definition</Typography>
                        <IconButton style={{outline: 'none'}} onClick={() => {setParams({ ...params, openSettings: true})}}><SettingsIcon /></IconButton>
                    </Toolbar>
                </AppBar>
            </Grid>

            <Grid item container style={{paddingTop: '64px'}} direction="column" justify='space-around'>
                <img src={infos.imageUrl} style={{margin: 'auto', maxWidth: '50%', maxHeight: '50%'}} alt='Icon'/>
                <div>{infos.content}</div>
            </Grid>

            <UpdateForm/>
        </Grid>
    )
}