import React, { useState, useEffect } from 'react';
import axios from 'axios';

import Toolbar from '@material-ui/core/Toolbar';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import AppBar from '@material-ui/core/AppBar';

import Paper from '@material-ui/core/Paper';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';

import Dialog from '@material-ui/core/Dialog';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';

import IconButton from '@material-ui/core/IconButton';
import RefreshIcon from '@material-ui/icons/Refresh';
import SettingsIcon from '@material-ui/icons/Settings';

import GetTimer from '../Forms/GetTimer'

import { BoardContext } from '../Dashboard';

import uuid from 'uuid'

export default function Grades(props) {
    const [params, setParams] = useState({ openSettings: false, ...props.infos, interval: null });
    const [infos, setInfos] = useState({grades: [{}]});

    const updateInfos = (login) => {
        axios.get(`http://localhost:8080/api/epitech/grade?user=${login}`)
        .then(({data}) => {console.log(data); setInfos({...data})})
    };

    useEffect(() => {
        updateInfos(params.login)

        params.interval = setInterval(() => updateInfos(params.login), 1000 * (params.hours * 3600 + params.minutes * 60 + params.seconds))
        // eslint-disable-next-line
    }, []);

    const UpdateForm = () => {
        const [boardState, setBoardState] = React.useContext(BoardContext);
        const [formState, setFormState] = useState({...params})

        const onConfirm = () => {
            formState.openSettings = false
            clearInterval(params.interval)
            setParams({
                ...formState,
                openSettings: false,
                interval: setInterval(() => updateInfos(formState.login), 1000 * (formState.hours * 3600 + formState.minutes * 60 + formState.seconds)),
            })
            updateInfos(formState.login)
        }

        const onCancel = () => {
            setFormState({...params})
            setParams({...params, openSettings: false})
        }

        const onDelete = () => {
            clearInterval(params.interval)
            const widgets = boardState.widgets.filter(({id}) => {return id !== params.id})
            setBoardState({
                widgets
            })
            setParams({...params, openSettings: false})
        }

        return (
            <Dialog open={params.openSettings}  onClose={onCancel}>
                <DialogTitle id="form-dialog-title">Epitech User's Grades</DialogTitle>
                <DialogContent>
                    <DialogContentText>This Widget needs a login</DialogContentText>
                    <TextField
                        autoFocus
                        margin="dense"
                        id="city"
                        label="City"
                        type="text"
                        fullWidth
                        value={formState.login}
                        onChange={event => setFormState({...formState, login: event.target.value})}
                    />
                </DialogContent>

                <GetTimer state={formState} setState={setFormState} />

                <DialogActions>
                    <Button onClick={onConfirm} style={{outline: 'none'}}>
                        Confirm
                    </Button>
                    <Button onClick={onCancel} style={{outline: 'none'}}>
                        Cancel
                    </Button>
                    <Button onClick={onDelete} style={{outline: 'none', color: 'red'}}>
                        Delete
                    </Button>
                </DialogActions>
            </Dialog>
        )
    }


    const StickyHeadTable = () => {
        return (
            <Paper style={{width: '100%'}}>
                <div style={{height: '50%', overflow: 'auto'}}>
                    <Table stickyHeader>
                    <TableHead>
                        <TableRow>
                            <TableCell key={'module'} align={'left'} style={{ minWidth: '60%' }}>{'module'}</TableCell>
                            <TableCell key={'grade'} align={'center'} style={{ minWidth: '20%' }}>{'grade'}</TableCell>
                            <TableCell key={'credits'} align={'center'} style={{ minWidth: '20%' }}>{'credits'}</TableCell>
                        </TableRow>
                    </TableHead>

                    <TableBody>
                        {infos.grades.map(({module, grade, credits}) => {
                            return (
                                <TableRow hover role="checkbox" tabIndex={-1} key={uuid()}>
                                        <TableCell key={'module'} align={'left'}>{module}</TableCell>
                                        <TableCell key={'grade'} align={'center'}>{grade}</TableCell>
                                        <TableCell key={'credits'} align={'center'}>{credits}</TableCell>
                                </TableRow>
                            );
                        })}
                    </TableBody>
                    </Table>
                </div>
            </Paper>
        );
    }

    return (
        <div>
            <Grid container style={{height: '100%'}} direction="column" justify='space-between'>
                <Grid item>
                    <AppBar>
                        <Toolbar style={{justifyContent: 'space-between'}}>
                            <IconButton style={{outline: 'none'}} onClick={() => updateInfos(params.login)}><RefreshIcon /></IconButton>
                            <Typography variant='h6'>Epitech User's Grades</Typography>
                            <IconButton style={{outline: 'none'}} onClick={() => {setParams({ ...params, openSettings: true})}}><SettingsIcon /></IconButton>
                        </Toolbar>
                    </AppBar>
                </Grid>

                <Grid item container style={{paddingTop: '64px'}}>
                    <StickyHeadTable />
                </Grid>
            </Grid>

            <UpdateForm/>
        </div>
    )
}