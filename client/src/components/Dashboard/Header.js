import React from 'react';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import IconButton from '@material-ui/core/IconButton';
import AddIcon from '@material-ui/icons/Add';
import QuitIcon from '@material-ui/icons/ExitToApp';

import { DrawerContext } from './Dashboard'

export default function Header() {
    // eslint-disable-next-line
    const [state, setState] = React.useContext(DrawerContext);
    return (
        <AppBar position='static'>
            <Toolbar style={{justifyContent: 'space-between'}}>

            <IconButton edge='start' color='inherit' style={{outline: 'none'}}
            onClick={() => setState(state => ({...state, openDrawer: !state.openDrawer}))}
            >
                <AddIcon />
            </IconButton>

            <Typography variant='h6' flexgrow='1'>
                Dashboard
            </Typography>

            <IconButton color='inherit' style={{outline: 'none'}} href='http://localhost:8080/api/auth/logout'>
                <QuitIcon />
            </IconButton>

            </Toolbar>
        </AppBar>
    )
};