import React, { Component } from 'react'
import axios from 'axios';

class Login extends Component {
    constructor(props) {
        super(props)
        this.authGoogle = this.authGoogle.bind(this)
    }

    authGoogle() {
        console.log('click')
        axios.get('http://localhost:8080/api/auth/google')
    }

    render() {
        return (
            <div className="container">
                <div class="row mt-5">
                    <div class="col-md-6 m-auto">
                        <div class="card card-body">
                            <h1 class="text-center mb-3"><i class="fas fa-sign-in-alt"></i>  Login</h1>
                            <form action="/api/auth/login" method="POST">
                                <div class="form-group">
                                    <label for="email">Email</label>
                                    <input
                                        type="email"
                                        id="email"
                                        name="email"
                                        class="form-control"
                                        placeholder="Enter Email"
                                    />
                                </div>
                                <div class="form-group">
                                    <label for="password">Password</label>
                                    <input
                                        type="password"
                                        id="password"
                                        name="password"
                                        class="form-control"
                                        placeholder="Enter Password"
                                    />
                                </div>
                                <button type="submit" class="btn btn-primary btn-block">Login</button>
                            </form>
                            <label class="spacedLabel" style={{textAlign:"center"}}>Or sign up using</label>
                            <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet"></link>
                            <a class="btn btn-block btn-social btn-google" href="http://localhost:8080/api/auth/google">
                                <span class="fa fa-google"></span> Sign in with Google
                            </a>
                            <a class="btn btn-block btn-social btn-facebook" href="http://localhost:8080/api/auth/facebook">
                                <span class="fa fa-facebook"></span> Sign in with Facebook
                            </a>
                            <a class="btn btn-block btn-social btn-github" href="http://localhost:8080/api/auth/github">
                                <span class="fa fa-github"></span> Sign in with Github
                            </a>
                            <p class="lead mt-4">
                                No Account? <a href="/register">Register</a>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
};

export default Login;