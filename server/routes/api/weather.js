const express = require('express');
const router = express.Router();
const OpenWeatherMapHelper = require("openweathermap-node");

const helper = new OpenWeatherMapHelper(
	{
		APPID: '754bcbe17d3f13b99c363ed932a3e4ee',
		units: "metric"
	}
);

/**
 * @swagger
 * /api/weather/simple:
 *   get:
 *     tags:
 *       - Weather
 *     description: Get simple weather-related informations for a given city (temperature, city and icon)
 *     parameters:
 *       - in: query
 *         name: city
 *         description: city
 *         required: true
 *         type: string
 *     responses:
 *        '200':
 *          description: JSON with simple weather informations for the given city
 *        '404':
 *          description: Unknown city
 */
router.get('/simple', (req, res) => {
    let city = req.query.city;

    helper.getCurrentWeatherByCityName(city, (err, weather) => {
        if (err) {
            return res.status(404).send(err);
        }
        else {
            let response = {temperature: weather.main.temp + " °C",
                            city: weather.name,
                            icon: "http://openweathermap.org/img/w/" + weather.weather[0].icon + ".png"}

            res.send(response)
        }
    });
});

/**
 * @swagger
 * /api/weather/complex:
 *   get:
 *     tags:
 *       - Weather
 *     description: Get complex weather-related informations for a given city
 *     parameters:
 *       - in: query
 *         name: city
 *         description: city
 *         required: true
 *         type: string
 *     responses:
 *        '200':
 *          description: JSON with complex weather informations for the given city
 *        '404':
 *          description: Unknown city
 */
router.get('/complex', (req, res) => {
    let city = req.query.city;

    helper.getCurrentWeatherByCityName(city, (err, weather) => {
        if (err) {
            return res.status(404).send(err);
        }
        else {
            let response = {temperature: weather.main.temp + " °C",
                            temperature_min: weather.main.temp_min + " °C",
                            temperature_max: weather.main.temp_max + " °C",
                            city: weather.name,
                            icon: "http://openweathermap.org/img/w/" + weather.weather[0].icon + ".png",
                            description: weather.weather[0].description,
                            pressure: weather.main.pressure + " hPa",
                            humidity: weather.main.humidity + "%",
                            wind: weather.wind.speed + "m/s"}

            res.send(response)
        }
    });
});

/**
 * @swagger
 * /api/weather/forecast:
 *   get:
 *     tags:
 *       - Weather
 *     description: Get array of weathers for 3 hours forecast
 *     parameters:
 *       - in: query
 *         name: city
 *         description: city
 *         required: true
 *         type: string
 *     responses:
 *        '200':
 *          description: JSON with an array of weathers for 3 hours forecast
 *        '404':
 *          description: Unknown city
 */

router.get('/forecast', (req, res) => {
    let city = req.query.city;

    helper.getThreeHourForecastByCityName(city, (err, weather) => {
        if (err) {
            return res.status(404).send(err);
        }
        else {
            let response = {
                city: city,
                temperatureList: weather.list
            }

            res.send(response)
        }
    });
});

module.exports = router