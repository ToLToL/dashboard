const express = require('express');
const router = express.Router();
const wiki = require('wikijs').default;

/**
 * @swagger
 * /api/wikipedia/article:
 *   get:
 *     tags:
 *       - Wikipedia
 *     description: Get main content and image url of a wikipedia article
 *     parameters:
 *       - in: query
 *         name: search
 *         description: article name
 *         required: true
 *         type: string
 *     responses:
 *        '200':
 *          description: Success
 *        '404':
 *          description: Article not found
 */
router.get('/article', (req, res) => {
    var content;

    wiki().page(req.query.search)
        .then(page => page.summary())
        .then(content => {
            content = content
            wiki().page(req.query.search)
            .then(page => page.mainImage())
            .then(image => {
                res.send({
                    imageUrl: image,
                    content: content
                })
            })
            .catch((error) => {
                res.status(404).send({message: 'No article found'})
            });
        })
        .catch((error) => {
            res.status(404).send({message: 'No article found'})
        });
})

module.exports = router;