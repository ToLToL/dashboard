const express = require('express');
const router = express.Router();
const YouTube = require('simple-youtube-api');
const youtube = new YouTube('AIzaSyB5y-4HTc6aqsppKuy_CZ-dpqhgsHcUb3U');

/**
 * @swagger
 * /api/youtube/channel:
 *   get:
 *     tags:
 *       - Youtube
 *     description: Get informations about a channel
 *     parameters:
 *       - in: query
 *         name: url
 *         description: url of a channel
 *         required: true
 *         type: string
 *     responses:
 *        '200':
 *          description: Success
 *        '404':
 *          description: Invalid channel url
 */
router.get('/channel', (req, res) => {
    var imageUrl, title, description;
    const options = {
        part: 'statistics'
    }
    youtube.getChannel(req.query.url)
    .then(channel => {
        if (channel) {
            imageUrl = channel['thumbnails']['medium']['url']
            title = channel['title']
            description = channel['description']
            youtube.getChannel(req.query.url, options)
            .then(channel => {
                if (channel) {
                    return res.send({
                        title: title,
                        description: description,
                        imageUrl: imageUrl,
                        viewCount: channel['viewCount'],
                        subscriberCount: channel['subscriberCount'],
                        videoCount: channel['videoCount']
                    })
                }
                else {
                    return res.status(404).send({message: 'Invalid channel url'})
                }
            })
            .catch((error) => {
                return res.status(404).send({message: 'Invalid channel url'})
            })
        }
        else {
            return res.status(404).send({message: 'Invalid channel url'});
        }
    })
    .catch((error) => {
        return res.status(404).send({message: 'Invalid channel url'})
    })
    }
)

/**
 * @swagger
 * /api/youtube/video:
 *   get:
 *     tags:
 *       - Youtube
 *     description: Get informations about a video
 *     parameters:
 *       - in: query
 *         name: url
 *         description: url of a video
 *         required: true
 *         type: string
 *     responses:
 *        '200':
 *          description: Success
 *        '404':
 *          description: Invalid video url
 */
router.get('/video', (req, res) => {
    var imageUrl, title, description;
    const options = {
        part: 'statistics'
    }
    youtube.getVideo(req.query.url)
    .then(video => {
        if (video) {
            imageUrl = video['thumbnails']['medium']['url']
            title = video['title']
            description = video['description']
            youtube.getVideo(req.query.url, options)
            .then(video => {
                if (video) {
                    res.send({
                        imageUrl: imageUrl,
                        title: title,
                        description: description,
                        viewCount: video['raw']['statistics']['viewCount'],
                        likeCount: video['raw']['statistics']['likeCount'],
                        dislikeCount: video['raw']['statistics']['dislikeCount'],
                        favoriteCount: video['raw']['statistics']['favoriteCount'],
                        commentCount: video['raw']['statistics']['commentCount']
                    })
                }
                else {
                    return res.status(404).send({message: 'Invalid video url'})
                }
            })
            .catch((error) => {
                return res.status(404).send({message: 'Invalid video url'})
            })
        }
        else {
            return res.status(404).send({message: 'Invalid video url'})
        }
    })
    .catch((error) => {
        return res.status(404).send({message: 'Invalid video url'})
    })
    }
)

module.exports = router;