const express = require('express');
const bcrypt = require('bcryptjs');
const passport = require('passport');
const LocalStrategy = require('passport-local').Strategy;
const GoogleStrategy = require('passport-google-oauth').OAuth2Strategy;
const FacebookStrategy = require('passport-facebook').Strategy;
const GitHubStrategy = require('passport-github').Strategy;
const passportConfig = require('../../config/passport');
const router = express.Router();

const User = require('../../models/User');

// Passport Serialize - Deserialize
passport.serializeUser((user, done) => {
    console.log('serialize')
    done(null, user)
})
passport.deserializeUser((user, done) => {
    console.log('deserialize')
    done(null, user)
})

// Passport Local
passport.use(new LocalStrategy({
    usernameField: 'email',
    passwordField: 'password'
    },
    function(email, password, done) {
      User.findOne({ email: email }, async (err, user) => {
        console.log(user.email)
        console.log(password)
        if (err) { return done(err); }
        if (!user) {
          return done(null, false, { message: 'Incorrect email.' });
        }
        if (!await bcrypt.compare(password, user.password)) {
          return done(null, false, { message: 'Incorrect password.' });
        }
        return done(null, user);
      });
    }
  ));

// Passport Google
passport.use(new GoogleStrategy({
  clientID        : passportConfig.google.clientID,
  clientSecret    : passportConfig.google.clientSecret,
  callbackURL     : passportConfig.google.callbackURL,
}, (accessToken, refreshToken, profile, done) => {
  var user = {
    username: profile.displayName,
    isOauth2: true,
    googleAccessToken: accessToken
  }
  User.findOneOrCreate({ username: profile.displayName }, user)
  return done(null, profile);
}));

// Passport Facebook
passport.use('facebook', new FacebookStrategy({
  clientID        : passportConfig.facebook.clientID,
  clientSecret    : passportConfig.facebook.clientSecret,
  callbackURL     : passportConfig.facebook.callbackURL,
  profileFields: passportConfig.facebook.profileFields
}, (accessToken, refreshToken, profile, done) => {
  var user = {
    username: profile.displayName,
    isOauth2: true,
    facebookAccessToken: accessToken
  }
  User.findOneOrCreate({ username: profile.displayName }, user)
  return done(null, profile);
}));

// Passport Github
passport.use('github', new GitHubStrategy({
  clientID: passportConfig.github.clientID,
  clientSecret: passportConfig.github.clientSecret,
  callbackURL: passportConfig.github.callbackURL
},
function(accessToken, refreshToken, profile, done) {
  var user = {
    username: profile.displayName,
    isOauth2: true,
    githubAccessToken: accessToken
  }
  User.findOneOrCreate({ username: profile.displayName }, user)
  return done(null, profile);
}));

/**
 * @swagger
 * /api/auth/register:
 *   post:
 *     tags:
 *       - Auth
 *     description: Register a new user
 *     parameters:
 *       - name: username
 *         description: username
 *         in: body
 *         required: true
 *         type: string
 *       - name: email
 *         description: email
 *         in: body
 *         required: true
 *         type: string
 *       - name: password
 *         description: password
 *         in: body
 *         required: true
 *         type: string
 */
router.post('/register', async (req, res) => {
    try {
        const hashedPassword = await bcrypt.hash(req.body.password, 10)
        var user = req.body

        user.isOauth2 = false
        user.password = hashedPassword
        await User.create(user)
        console.log(user)
        res.redirect('/login')
    } catch (error) {
        res.redirect('/register')
    }
});

/**
 * @swagger
 * /api/auth/login:
 *   post:
 *     tags:
 *       - Auth
 *     description: Login an user
 *     parameters:
 *       - name: email
 *         description: email
 *         in: body
 *         required: true
 *         type: string
 *       - name: password
 *         description: password
 *         in: body
 *         required: true
 *         type: string
 */
router.post('/login', passport.authenticate('local', {
    successRedirect: '/dashboard',
    failureRedirect: '/login',
    failureFlash: true
}))

router.get('/google', passport.authenticate('google', {
  scope: ['profile']
}))

router.get('/google/callback', 
  passport.authenticate('google', { failureRedirect: '/login' }),
  function(req, res) {
    res.redirect("http://localhost:80/dashboard")
});

router.get('/facebook',
  passport.authenticate('facebook', {
    authType: 'reauthenticate',
    scope : 'email'
}));

router.get('/facebook/callback',
  passport.authenticate('facebook', { failureRedirect: '/login' }),
  function(req, res) {
    res.redirect("http://localhost:80/dashboard")
});

router.get('/github',
  passport.authenticate('github', {
    scope : 'profile'
}));

router.get('/github/callback',
  passport.authenticate('github', { failureRedirect: '/login'}),
  function(req, res) {
    res.redirect("http://localhost:80/dashboard")
});

router.get('/logout', function(req, res) {
  console.log(req.user)
  req.logout();
  res.redirect('http://localhost:80');
});

module.exports = router;