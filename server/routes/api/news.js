const express = require('express');
const NewsAPI = require('newsapi');
const newsapi = new NewsAPI('e835e9448ab7418790ba21b74a80f202');
const router = express.Router();

/**
 * @swagger
 * /api/news:
 *   get:
 *     tags:
 *       - News
 *     description: Get news from different magazines / newspaper
 *     parameters:
 *       - in: query
 *         name: search
 *         description: search keyword
 *         required: true
 *         type: string
 *     responses:
 *        '200':
 *          description: Success
 */
router.get('/', (req, res) => {
    newsapi.v2.topHeadlines({
        q: req.query.search,
        language: 'fr',
        page: 1
    }).then(response => {
        res.send({
            totalResults: response['totalResults'],
            articles: response['articles']
        })
    });
})

module.exports = router;