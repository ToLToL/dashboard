const express = require('express');
const request = require('request');
const router = express.Router();

module.exports = router;

const url = 'https://intra.epitech.eu/auth-29b87619b8bd3e9aad5eba4c1355cff3b3923a0b'

/**
 * @swagger
 * /api/epitech/user:
 *   get:
 *     tags:
 *       - Epitech
 *     description: Get informations about a student
 *     parameters:
 *       - in: query
 *         name: user
 *         description: Student login
 *         required: true
 *         type: string
 *     responses:
 *        '200':
 *          description: Success
 *        '404':
 *          description: Error
 */
router.get('/user', (req, res) => {
    const options = {
        url: url + '/user/' + req.query.user,
        method: 'GET',
        qs: {
            'format': 'json'
        }
    }

    request(options, (err, response, body) => {
        console.log(response.statusCode)
        if (err || response.statusCode != 200) {
            return res.status(404).send({
                message: err
            })
        }
        const info = JSON.parse(body);
        res.send({fullName: info.title,
                picture: 'https://intra.epitech.eu' + info.picture,
                promo: info.promo,
                credit: info.credit,
                gpa: info.gpa})
    })
})

/**
 * @swagger
 * /api/epitech/binome:
 *   get:
 *     tags:
 *       - Epitech
 *     description: Get informations about a student's binomes
 *     parameters:
 *       - in: query
 *         name: user
 *         description: Student login
 *         required: true
 *         type: string
 *     responses:
 *        '200':
 *          description: Success
 *        '404':
 *          description: Error
 */
router.get('/binome', (req, res) => {
    const options = {
        url: url + '/user/' + req.query.user + '/binome/',
        method: 'GET',
        qs: {
            'format': 'json'
        }
    }
    request(options, (err, response, body) => {
        if (err || response.statusCode != 200) {
            return res.status(404).send({
                message: err
            })
        }
        const info = JSON.parse(body);
        var binomes = []
        info.binomes.forEach( (element) => {
            binomes.push({
                login: element.login,
                picture: 'https://intra.epitech.eu' + element.picture,
                nb_activies: element.nb_activities
            })
        })
        res.send({binomes: binomes})
    })
})

/**
 * @swagger
 * /api/epitech/grade:
 *   get:
 *     tags:
 *       - Epitech
 *     description: Get informations about a student's grades
 *     parameters:
 *       - in: query
 *         name: user
 *         description: Student login
 *         required: true
 *         type: string
 *     responses:
 *        '200':
 *          description: Success
 *        '404':
 *          description: Error
 */
router.get('/grade', (req, res) => {
    const options = {
        url: url + '/user/' + req.query.user + '/notes/',
        method: 'GET',
        qs: {
            'format': 'json'
        }
    }
    request(options, (err, response, body) => {
        if (err || response.statusCode != 200) {
            return res.status(404).send({
                message: err
            })
        }
        const info = JSON.parse(body);
        var grades = []
        info.modules.forEach( (element) => {
            grades.push({
                module: element.title,
                grade: element.grade,
                credits: element.credits
            })
        })
        res.send({grades: grades})
    })
})