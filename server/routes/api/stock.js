const express = require('express');
const request = require('request');
const router = express.Router();

const url = 'https://www.alphavantage.co/query'
const apikey = 'LEF0JPOX43GPUD5I'
let physicalCurrencyCodes = ['AED','AFN','ALL','AMD','ANG','AOA','ARS','AUD','AWG','AZN','BAM','BBD','BDT','BGN','BHD','BIF','BMD','BND','BOB','BRL','BSD','BTN','BWP','BZD','CAD','CDF','CHF','CLF','CLP','CNH','CNY','COP','CUP','CVE','CZK','DJF','DKK','DOP','DZD','EGP','ERN','ETB','EUR','FJD','FKP','GBP','GEL','GHS','GIP','GMD','GNF','GTQ','GYD','HKD','HNL','HRK','HTG','HUF','IDR','ILS','INR','IQD','IRR','ISK','JEP','JMD','JOD','JPY','KES','KGS','KHR','KMF','KPW','KRW','KWD','KYD','KZT','LAK','LBP','LKR','LRD','LSL','LYD','MAD','MDL','MGA','MKD','MMK','MNT','MOP','MRO','MRU','MUR','MVR','MWK','MXN','MYR','MZN','NAD','NGN','NOK','NPR','NZD','OMR','PAB','PEN','PGK','PHP','PKR','PLN','PYG','QAR','RON','RSD','RUB','RUR','RWF','SAR','SBDf','SCR','SDG','SEK','SGD','SHP','SLL','SOS','SRD','SYP','SZL','THB','TJS','TMT','TND','TOP','TRY','TTD','TWD','TZS','UAH','UGX','USD','UYU','UZS','VND','VUV','WST','XAF','XAG','XAU','XCD','XDR','XOF','XPF','YER','ZAR','ZMW','ZWL']

/**
 * @swagger
 * /api/stock/exchange/rates:
 *   get:
 *     tags:
 *       - Stock
 *     description: Get realtime currency exchange rate
 *     parameters:
 *       - in: query
 *         name: from_currency
 *         description: first currency
 *         required: true
 *         type: string
 *       - in: query
 *         name: to_currency
 *         description: second currency
 *         required: true
 *         type: string
 *     responses:
 *        '200':
 *          description: JSON with the 2 currency names and exchange rate
 *        '400':
 *          description: Invalid physical currency code
 */
router.get('/exchange/rates', (req, res) => {
    if (!physicalCurrencyCodes.includes(req.query.from_currency) || !physicalCurrencyCodes.includes(req.query.to_currency)) {
        return res.status(400).send({
            message: 'Invalid physical currency code'
        });
    }
    const options = {
        url: url,
        method: 'GET',
        qs: {
          'function': 'CURRENCY_EXCHANGE_RATE',
          'from_currency': req.query.from_currency,
          'to_currency': req.query.to_currency,
          'apikey': apikey
        }
      };
    request(options, (err, response, body) => {
        if (!err && response.statusCode == 200) {
            const info = JSON.parse(body);
            res.send({from_currency: info['Realtime Currency Exchange Rate']['1. From_Currency Code'],
                    to_currency: info['Realtime Currency Exchange Rate']['3. To_Currency Code'],
                    exchange_rate: info['Realtime Currency Exchange Rate']['5. Exchange Rate']
        })
    }})
});

/**
 * @swagger
 * /api/stock/exchange/daily:
 *   get:
 *     tags:
 *       - Stock
 *     description: Get daily evolution of the 2 currencies exchange rate
 *     parameters:
 *       - in: query
 *         name: from_currency
 *         description: first currency
 *         required: true
 *         type: string
 *       - in: query
 *         name: to_currency
 *         description: second currency
 *         required: true
 *         type: string
 *     responses:
 *        '200':
 *          description: JSON with daily evolution of the 2 currencies exchange rate
 *        '400':
 *          description: Invalid physical currency code
 */
router.get('/exchange/daily', (req, res) => {
    if (!physicalCurrencyCodes.includes(req.query.from_currency) || !physicalCurrencyCodes.includes(req.query.to_currency)) {
        return res.status(400).send({
            message: 'Invalid physical currency code'
        });
    }
    const options = {
        url: url,
        method: 'GET',
        qs: {
          'function': 'FX_DAILY',
          'from_symbol': req.query.from_currency,
          'to_symbol': req.query.to_currency,
          'apikey': apikey
        }
      };
    request(options, (err, response, body) => {
        if (!err && response.statusCode == 200) {
            const info = JSON.parse(body);
            res.send({from_currency: info['Meta Data']['2. From Symbol'],
                    to_currency: info['Meta Data']['3. To Symbol'],
                    evolution: info['Time Series FX (Daily)']
        })
    }})
});

/**
 * @swagger
 * /api/stock/exchange/weekly:
 *   get:
 *     tags:
 *       - Stock
 *     description: Get weekly evolution of the 2 currencies exchange rate
 *     parameters:
 *       - in: query
 *         name: from_currency
 *         description: first currency
 *         required: true
 *         type: string
 *       - in: query
 *         name: to_currency
 *         description: second currency
 *         required: true
 *         type: string
 *     responses:
 *        '200':
 *          description: JSON with weekly evolution of the 2 currencies exchange rate
 *        '400':
 *          description: Invalid physical currency code
 */
router.get('/exchange/weekly', (req, res) => {
    if (!physicalCurrencyCodes.includes(req.query.from_currency) || !physicalCurrencyCodes.includes(req.query.to_currency)) {
        return res.status(400).send({
            message: 'Invalid physical currency code'
        });
    }
    const options = {
        url: url,
        method: 'GET',
        qs: {
          'function': 'FX_WEEKLY',
          'from_symbol': req.query.from_currency,
          'to_symbol': req.query.to_currency,
          'apikey': apikey
        }
      };
    request(options, (err, response, body) => {
        if (!err && response.statusCode == 200) {
            const info = JSON.parse(body);
            res.send({from_currency: info['Meta Data']['2. From Symbol'],
                    to_currency: info['Meta Data']['3. To Symbol'],
                    evolution: info['Time Series FX (Weekly)']
        })
    }})
});

/**
 * @swagger
 * /api/stock/exchange/monthly:
 *   get:
 *     tags:
 *       - Stock
 *     description: Get monthly evolution of the 2 currencies exchange rate
 *     parameters:
 *       - in: query
 *         name: from_currency
 *         description: first currency
 *         required: true
 *         type: string
 *       - in: query
 *         name: to_currency
 *         description: second currency
 *         required: true
 *         type: string
 *     responses:
 *        '200':
 *          description: JSON with monthly evolution of the 2 currencies exchange rate
 *        '400':
 *          description: Invalid physical currency code
 */
router.get('/exchange/monthly', (req, res) => {
    if (!physicalCurrencyCodes.includes(req.query.from_currency) || !physicalCurrencyCodes.includes(req.query.to_currency)) {
        return res.status(400).send({
            message: 'Invalid physical currency code'
        });
    }
    const options = {
        url: url,
        method: 'GET',
        qs: {
          'function': 'FX_MONTHLY',
          'from_symbol': req.query.from_currency,
          'to_symbol': req.query.to_currency,
          'apikey': apikey
        }
      };
    request(options, (err, response, body) => {
        if (!err && response.statusCode == 200) {
            const info = JSON.parse(body);
            res.send({from_currency: info['Meta Data']['2. From Symbol'],
                    to_currency: info['Meta Data']['3. To Symbol'],
                    evolution: info['Time Series FX (Monthly)']
        })
    }})
});

/**
 * @swagger
 * /api/stock/equity/daily:
 *   get:
 *     tags:
 *       - Stock
 *     description: Get daily evolution of an equity
 *     parameters:
 *       - in: query
 *         name: equity
 *         description: equity symbol
 *         required: true
 *         type: string
 *     responses:
 *        '200':
 *          description: JSON with daily evolution of the equity
 *        '400':
 *          description: Invalid equity symbol
 */
router.get('/equity/daily', (req, res) => {
    const options = {
        url: url,
        method: 'GET',
        qs: {
          'function': 'TIME_SERIES_DAILY',
          'symbol': req.query.equity,
          'apikey': apikey
        }
      };
    request(options, (err, response, body) => {
        const info = JSON.parse(body);
        if (err || info['Error Message']) {
            return res.status(400).send({
                message: 'Invalid equity symbol'
            })
        }
        console.log(info);
        res.send({equity: info['Meta Data']['2. Symbol'],
                evolution: info['Time Series (Daily)']
        })
    })
});

/**
 * @swagger
 * /api/stock/equity/weekly:
 *   get:
 *     tags:
 *       - Stock
 *     description: Get weekly evolution of an equity
 *     parameters:
 *       - in: query
 *         name: equity
 *         description: equity symbol
 *         required: true
 *         type: string
 *     responses:
 *        '200':
 *          description: JSON with weekly evolution of the equity
 *        '400':
 *          description: Invalid equity symbol
 */
router.get('/equity/weekly', (req, res) => {
    const options = {
        url: url,
        method: 'GET',
        qs: {
          'function': 'TIME_SERIES_WEEKLY',
          'symbol': req.query.equity,
          'apikey': apikey
        }
      };
    request(options, (err, response, body) => {
        const info = JSON.parse(body);
        if (err || info['Error Message']) {
            return res.status(400).send({
                message: 'Invalid equity symbol'
            })
        }
        console.log(info);
        res.send({equity: info['Meta Data']['2. Symbol'],
                evolution: info['Weekly Time Series']
        })
    })
});

/**
 * @swagger
 * /api/stock/equity/monthly:
 *   get:
 *     tags:
 *       - Stock
 *     description: Get monthly evolution of an equity
 *     parameters:
 *       - in: query
 *         name: equity
 *         description: equity symbol
 *         required: true
 *         type: string
 *     responses:
 *        '200':
 *          description: JSON with monthly evolution of the equity
 *        '400':
 *          description: Invalid equity symbol
 */
router.get('/equity/monthly', (req, res) => {
    const options = {
        url: url,
        method: 'GET',
        qs: {
          'function': 'TIME_SERIES_MONTHLY',
          'symbol': req.query.equity,
          'apikey': apikey
        }
      };
    request(options, (err, response, body) => {
        const info = JSON.parse(body);
        if (err || info['Error Message']) {
            return res.status(400).send({
                message: 'Invalid equity symbol'
            })
        }
        console.log(info);
        res.send({equity: info['Meta Data']['2. Symbol'],
                evolution: info['Monthly Time Series']
        })
    })
});

module.exports = router