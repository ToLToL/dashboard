const express = require('express');
global.fetch = require('node-fetch')
const cc = require('cryptocompare')
cc.setApiKey('bc9d319b6c586ffe7f2e6ccffaaad7c676e384872f4d5db6e7e9efc27b33af00')
const router = express.Router();

/**
 * @swagger
 * /api/crypto:
 *   get:
 *     tags:
 *       - Crypto
 *     description: Get crypto's exchange rate
 *     parameters:
 *       - in: query
 *         name: from_crypto
 *         description: first currency
 *         required: true
 *         type: string
 *       - in: query
 *         name: to_currency
 *         description: second currency
 *         required: true
 *         type: string
 *     responses:
 *        '200':
 *          description: Success
 *        '400':
 *          description: Invalid crypto/currency symbol
 */
router.get('/', (req, res) => {
    const to_currency = req.query.to_currency.toUpperCase()
    cc.price(req.query.from_crypto, req.query.to_currency)
    .then(prices => {
        console.log(prices)
        res.send({
            exchangeRate: prices[to_currency]
        })
    })
    .catch((error) => {
        res.status(404).send({message: 'Invalid crypto/currency symbol'})
    })
})

module.exports = router;