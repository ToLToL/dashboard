const express = require('express');
const GitHub = require('github-api');
const router = express.Router();

var gh, me;

/**
 * @swagger
 * /api/github/login:
 *   get:
 *     tags:
 *       - Github
 *     description: Login with github username and password
 *     parameters:
 *       - in: query
 *         name: username
 *         description: github username
 *         required: true
 *         type: string
 *       - in: query
 *         name: password
 *         description: github password
 *         required: true
 *         type: string
 *     responses:
 *        '200':
 *          description: Logged in successfully
 *        '400':
 *          description: Invalid username/password
 */
router.get('/login', (req, res) => {
    gh = new GitHub({
        username: req.query.username,
        password: req.query.password
    });
    me = gh.getUser()
    res.send({message: 'Logged in'})
})

/**
 * @swagger
 * /api/github/repos:
 *   get:
 *     tags:
 *       - Github
 *     description: Get user's repos
 *     responses:
 *        '200':
 *          description: Send JSON with repo name, url, number of stars/watch/forks
 *        '400':
 *          description: Not logged in
 */
router.get('/repos', (req, res) => {
    if (!me) {
        return res.status(400).send({
            message: 'Please log in'
        })
    }
    const options = {
        type: 'owner'
    }
    console.log(me)
    me.listRepos(options, (err, repos) => {
        var repoList = []
        repos.forEach(element => {
            repoList.push({
                name: element.name,
                url: element.html_url,
                stars: element.stargazers_count,
                watchers_count: element.watchers_count,
                forks: element.forks
            })
        });
        console.log(repoList)
        res.send(repoList)
    })
})

/**
 * @swagger
 * /api/github/create/repo:
 *   get:
 *     tags:
 *       - Github
 *     description: Create a repo
 *     parameters:
 *       - in: query
 *         name: name
 *         description: repo name
 *         required: true
 *         type: string
 *       - in: query
 *         name: description
 *         description: repo description
 *         required: true
 *         type: string
 *       - in: query
 *         name: private
 *         description: should the repo be private or public?
 *         required: true
 *         type: boolean
 *       - in: query
 *         name: auto_init
 *         description: add an auto_init to the repo?
 *         required: true
 *         type: boolean
 *     responses:
 *        '200':
 *          description: Send JSON with informations about the new created repo
 *        '400':
 *          description: Not logged in
 */
router.get('/create/repo', (req, res) => {
    if (!me) {
        return res.status(400).send({
            message: 'Please log in'
        })
    }
    const options = {
        name: req.query.name,
        description: req.query.description,
        private: req.query.private,
        auto_init: req.query.auto_init
    }
    me.createRepo(options, (err, response) => {
        console.log(response)
        res.send(response)
    })
})

module.exports = router;