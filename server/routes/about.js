const express = require('express');
const router = express.Router();

function pushService(services, serviceName, widgets) {
    services.push({
        name: serviceName,
        widgets: widgets
    })
}

function addServices(services) {
    pushService(services, "Weather", [{
        name: "Simple city temperature",
        description: "Display simple temperature informations for a city",
        params: [{
            name: "city",
            type: "string"
        }]},
        {
        name: "Complex city temperature",
        description: "Display complex temperature informations for a city",
        params: [{
            name: "city",
            type: "string"
        }]},
        {
        name: "City temperature forecast",
        description: "Display temperature forecast for a city",
        params: [{
            name: "city",
            type: "string"
        }]
    }])
    pushService(services, "Epitech", [{
        name: "Student information",
        description: "Display informations about a student",
        params: [{
            name: "user",
            type: "string"
        }]},
        {
        name: "Student's binomes",
        description: "Display informations about a student's binomes",
        params: [{
            name: "user",
            type: "string"
        }]},
        {
        name: "Student's grades",
        description: "Display grades of a student",
        params: [{
            name: "user",
            type: "string"
        }]
    }])
    pushService(services, "Stock", [{
        name: "Exchange rates",
        description: "Display realtime currency exchange rate",
        params: [{
            name: "from_currency",
            type: "string"
        },
        {
            name: "to_currency",
            type: "string"
        }]},
        {
        name: "Daily - exchange rates between 2 currencies",
        description: "Display daily, realtime exchange rates between 2 currencies",
        params: [{
            name: "from_currency",
            type: "string"
        },
        {
            name: "to_currency",
            type: "string"
        }]},
        {
        name: "Weekly - exchange rates between 2 currencies",
        description: "Display weekly, realtime exchange rates between 2 currencies",
        params: [{
            name: "from_currency",
            type: "string"
        },
        {
            name: "to_currency",
            type: "string"
        }]},
        {
        name: "Monthly - exchange rates between 2 currencies",
        description: "Display monthly, realtime exchange rates between 2 currencies",
        params: [{
            name: "from_currency",
            type: "string"
        },
        {
            name: "to_currency",
            type: "string"
        }]},
        {
        name: "Daily - equity",
        description: "Display daily, realtime evolution of an equity",
        params: [{
            name: "equity",
            type: "string"
        }]},
        {
        name: "Weekly - equity",
        description: "Display weekly, realtime evolution of an equity",
        params: [{
            name: "equity",
            type: "string"
        }]},
        {
        name: "Monthly - equity",
        description: "Display monthly, realtime evolution of an equity",
        params: [{
            name: "equity",
            type: "string"
        }]},
    ])
    pushService(services, "Github", [{
        name: "User repositories",
        description: "Display user's owned repositories",
        params: []
        },
        {
        name: "Create a repository",
        description: "Create a repository on the user's account",
        params: [{
            name: "name",
            type: "string"
        },
        {
            name: "description",
            type: "string"
        },
        {
            name: "private",
            type: "boolean"
        },
        {
            name: "auto_init",
            type: "boolean"
        }]
    }])
    pushService(services, "Crypto", [{
        name: "Crypto exchange rate",
        description: "Display exchange rate of a crypto currency",
        params: [{
            name: "from_crypto",
            type: "string"
        },
        {
            name: "to_crypto",
            type: "string"
        }]
    }])
    pushService(services, "Wikipedia", [{
        name: "Youtube channel",
        description: "Display informations about a Youtube channel",
        params: [{
            name: "url",
            type: "string"
        }]
    }])
    pushService(services, "Youtube", [{
        name: "Youtube video",
        description: "Display informations about a Youtube video",
        params: [{
            name: "url",
            type: "string"
        }]
    }])
}

router.get('/', (req, res) => {
    var clientIp = req.headers['x-forwarded-for'] || req.connection.remoteAddress;
    if (clientIp.substr(0, 7) == "::ffff:") {
        clientIp = clientIp.substr(7)
    }

    var services = []
    addServices(services)
    res.json(
        {
            "client": {
                "host": clientIp
            },
            "server": {
                "current_time": Date.now(),
                "services": services
            }
        }
    )
});

module.exports = router;