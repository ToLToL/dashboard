const mongoose = require('mongoose');

// Create Schema
const UserSchema = new mongoose.Schema({
    username: {
        type: String,
        required: true
    },
    email: {
        type: String,
        required: false
    },
    password: {
        type: String,
        required: false
    },
    isOauth2: {
        type: Boolean,
        required: true
    },
    facebookAccessToken: {
        type: String,
        required: false
    },
    googleAccessToken: {
        type: String,
        required: false
    },
    githubAccessToken: {
        type: String,
        required: false
    }
});

UserSchema.static('findOneOrCreate', async function findOneOrCreate(condition, doc) {
    const one = await this.findOne(condition);
  
    return one || this.create(doc);
  });

module.exports = User = mongoose.model('User', UserSchema);