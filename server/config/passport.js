const passportConfig = {
    facebook: {
        clientID: 2492561387694116,
        clientSecret: 'b280ec6e07725dcd12e44db6e38eb066',
        callbackURL: 'http://localhost:8080/api/auth/facebook/callback',
        profileFields: ['email', 'displayName']
    },
    google: {
        callbackURL: 'http://localhost:8080/api/auth/google/callback',
        clientID: '698771012207-4dce2k9jtjupd7u3pf0itp0itlcdg9n4.apps.googleusercontent.com',
        clientSecret: '77RHphagMiV67vSGrLxoUre0'
    },
    github: {
        callbackURL: 'http://localhost:8080/api/auth/github/callback',
        clientID: '4ab8d4e8aef9174a1be8',
        clientSecret: 'bc39a431c1188fcb5d5635bbc308326ed8bd225a'
    }
};

module.exports = passportConfig;