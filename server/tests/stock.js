const app = require("../server.js")
const request = require('supertest');
const expect = require('chai').expect

describe('=== API: /api/stock/exchange/rates |from_currency=USD to_currency=EUR| ===', function() {
    this.timeout(10000)
    it('Should send back a JSON with exchange rates informations', function(done) {
      request(app)
        .get('/api/stock/exchange/rates')
        .set('Content-Type', 'application/json')
        .query({from_currency: 'USD', to_currency: 'EUR'})
        .expect('Content-Type', /json/)
        .expect(200, function(err, res) {
          if (err) { return done(err); }
          expect(res.body.from_currency).to.equal('USD');
          expect(res.body.to_currency).to.equal('EUR');
          expect(res.body.exchange_rate).to.not.be.null;
          done();
        });
    });
});

describe('=== API: /api/stock/exchange/rates |from_currency=EUR to_currency=USD| ===', function() {
    this.timeout(10000)
    it('Should send back a JSON with exchange rates informations', function(done) {
      request(app)
        .get('/api/stock/exchange/rates')
        .set('Content-Type', 'application/json')
        .query({from_currency: 'EUR', to_currency: 'USD'})
        .expect('Content-Type', /json/)
        .expect(200, function(err, res) {
          if (err) { return done(err); }
          expect(res.body.from_currency).to.equal('EUR');
          expect(res.body.to_currency).to.equal('USD');
          expect(res.body.exchange_rate).to.not.be.null;
          done();
        });
    });
});

describe('=== API: /api/stock/exchange/rates |from_currency=unknownCurrency to_currency=USD| ===', function() {
    this.timeout(10000)
    it('Should send back a JSON with exchange rates informations', function(done) {
      request(app)
        .get('/api/stock/exchange/rates')
        .set('Content-Type', 'application/json')
        .query({from_currency: 'unknownCurrency', to_currency: 'USD'})
        .expect('Content-Type', /json/)
        .expect(400, function(err, res) {
            if (err) { return done(err); }
            done();
        });
    });
});

describe('=== API: /api/stock/exchange/daily |from_currency=EUR to_currency=USD| ===', function() {
    this.timeout(10000)
    it('Should send back a JSON with daily exchange rates informations', function(done) {
      request(app)
        .get('/api/stock/exchange/daily')
        .set('Content-Type', 'application/json')
        .query({from_currency: 'EUR', to_currency: 'USD'})
        .expect('Content-Type', /json/)
        .expect(200, function(err, res) {
          if (err) { return done(err); }
          expect(res.body.from_currency).to.equal('EUR');
          expect(res.body.to_currency).to.equal('USD');
          expect(res.body.evolution).to.not.be.null;
          done();
        });
    });
});

// describe('=== API: /api/stock/exchange/daily |from_currency=USD to_currency=EUR| ===', function() {
//     this.timeout(10000)
//     it('Should send back a JSON with daily exchange rates informations', function(done) {
//       request(app)
//         .get('/api/stock/exchange/daily')
//         .set('Content-Type', 'application/json')
//         .query({from_currency: 'USD', to_currency: 'EUR'})
//         .expect('Content-Type', /json/)
//         .expect(200, function(err, res) {
//           if (err) { return done(err); }
//           expect(res.body.from_currency).to.equal('USD');
//           expect(res.body.to_currency).to.equal('EUR');
//           expect(res.body.evolution).to.not.be.null;
//           done();
//         });
//     });
// });

describe('=== API: /api/stock/exchange/daily |from_currency=unknownCurrency to_currency=EUR| ===', function() {
    this.timeout(10000)
    it('Should send back a JSON with daily exchange rates informations', function(done) {
      request(app)
        .get('/api/stock/exchange/daily')
        .set('Content-Type', 'application/json')
        .query({from_currency: 'unknownCurrency', to_currency: 'EUR'})
        .expect('Content-Type', /json/)
        .expect(400, function(err, res) {
          if (err) { return done(err); }
          done();
        });
    });
});

describe('=== API: /api/stock/exchange/weekly |from_currency=USD to_currency=EUR| ===', function() {
    this.timeout(10000)
    it('Should send back a JSON with weekly exchange rates informations', function(done) {
      request(app)
        .get('/api/stock/exchange/weekly')
        .set('Content-Type', 'application/json')
        .query({from_currency: 'USD', to_currency: 'EUR'})
        .expect('Content-Type', /json/)
        .expect(200, function(err, res) {
          if (err) { return done(err); }
          expect(res.body.from_currency).to.equal('USD');
          expect(res.body.to_currency).to.equal('EUR');
          expect(res.body.evolution).to.not.be.null;
          done();
        });
    });
});

// describe('=== API: /api/stock/exchange/weekly |from_currency=EUR to_currency=USD| ===', function() {
//     this.timeout(10000)
//     it('Should send back a JSON with weekly exchange rates informations', function(done) {
//       request(app)
//         .get('/api/stock/exchange/weekly')
//         .set('Content-Type', 'application/json')
//         .query({from_currency: 'EUR', to_currency: 'USD'})
//         .expect('Content-Type', /json/)
//         .expect(200, function(err, res) {
//           if (err) { return done(err); }
//           expect(res.body.from_currency).to.equal('EUR');
//           expect(res.body.to_currency).to.equal('USD');
//           expect(res.body.evolution).to.not.be.null;
//           done();
//         });
//     });
// });

describe('=== API: /api/stock/exchange/weekly |from_currency=unknownCurrency to_currency=USD| ===', function() {
    this.timeout(10000)
    it('Should send back a JSON with weekly exchange rates informations', function(done) {
      request(app)
        .get('/api/stock/exchange/weekly')
        .set('Content-Type', 'application/json')
        .query({from_currency: 'unknownCurrency', to_currency: 'USD'})
        .expect('Content-Type', /json/)
        .expect(400, function(err, res) {
          if (err) { return done(err); }
          done();
        });
    });
});

describe('=== API: /api/stock/exchange/monthly |from_currency=USD to_currency=EUR| ===', function() {
    this.timeout(10000)
    it('Should send back a JSON with monthly exchange rates informations', function(done) {
      request(app)
        .get('/api/stock/exchange/monthly')
        .set('Content-Type', 'application/json')
        .query({from_currency: 'USD', to_currency: 'EUR'})
        .expect('Content-Type', /json/)
        .expect(200, function(err, res) {
          if (err) { return done(err); }
          expect(res.body.from_currency).to.equal('USD');
          expect(res.body.to_currency).to.equal('EUR');
          expect(res.body.evolution).to.not.be.null;
          done();
        });
    });
});

// describe('=== API: /api/stock/exchange/monthly |from_currency=USD to_currency=EUR| ===', function() {
//     this.timeout(10000)
//     it('Should send back a JSON with monthly exchange rates informations', function(done) {
//       request(app)
//         .get('/api/stock/exchange/monthly')
//         .set('Content-Type', 'application/json')
//         .query({from_currency: 'USD', to_currency: 'EUR'})
//         .expect('Content-Type', /json/)
//         .expect(200, function(err, res) {
//           if (err) { return done(err); }
//           expect(res.body.from_currency).to.equal('USD');
//           expect(res.body.to_currency).to.equal('EUR');
//           expect(res.body.evolution).to.not.be.null;
//           done();
//         });
//     });
// });