const app = require("../server.js")
const request = require('supertest');
const expect = require('chai').expect

describe('=== API: api/crypto |from_crypto=BTC to_currency=EUR| ===', function() {
    this.timeout(10000)
    it('Should send back a JSON with exchange rate of the crypto money', function(done) {
      request(app)
        .get('/api/crypto')
        .set('Content-Type', 'application/json')
        .query({ from_crypto: 'BTC', to_currency: 'EUR'})
        .expect('Content-Type', /json/)
        .expect(200, function(err, res) {
          if (err) { return done(err); }
          done();
        });
    });
});

describe('=== API: api/crypto |from_crypto=BTC to_currency=USD| ===', function() {
    this.timeout(10000)
    it('Should send back a JSON with exchange rate of the crypto money', function(done) {
      request(app)
        .get('/api/crypto')
        .set('Content-Type', 'application/json')
        .query({ from_crypto: 'BTC', to_currency: 'USD'})
        .expect('Content-Type', /json/)
        .expect(200, function(err, res) {
          if (err) { return done(err); }
          done();
        });
    });
});

describe('=== API: api/crypto |from_crypto=BTC to_currency=TOTO| ===', function() {
    this.timeout(10000)
    it('Should send back a 404 error code', function(done) {
      request(app)
        .get('/api/crypto')
        .set('Content-Type', 'application/json')
        .query({ from_crypto: 'BTC', to_currency: 'TOTO'})
        .expect('Content-Type', /json/)
        .expect(404, function(err, res) {
          if (err) { return done(err); }
          done();
        });
    });
});