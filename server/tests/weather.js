const app = require("../server.js")
const request = require('supertest');
const expect = require('chai').expect

describe('=== API: api/weather/simple/ |city=paris| ===', function() {
    this.timeout(10000)
    it('Should send back a JSON simple temperature informations about a city', function(done) {
      request(app)
        .get('/api/weather/simple')
        .set('Content-Type', 'application/json')
        .query({ city: 'Paris'})
        .expect('Content-Type', /json/)
        .expect(200, function(err, res) {
          if (err) { return done(err); }
          expect(res.body.city).to.equal('Paris');
          expect(res.body.temperature).to.not.be.null;
          expect(res.body.icon).to.not.be.null;
          done();
        });
    });
});

describe('=== API: api/weather/simple/ |city=London| ===', function() {
  this.timeout(10000)
  it('Should send back a JSON simple temperature informations about a city', function(done) {
    request(app)
      .get('/api/weather/simple')
      .set('Content-Type', 'application/json')
      .query({ city: 'London'})
      .expect('Content-Type', /json/)
      .expect(200, function(err, res) {
        if (err) { return done(err); }
        expect(res.body.city).to.equal('London');
        expect(res.body.temperature).to.not.be.null;
        expect(res.body.icon).to.not.be.null;
        done();
      });
  });
});

describe('=== API: api/weather/simple/ |city=Madrid| ===', function() {
  this.timeout(10000)
  it('Should send back a JSON simple temperature informations about a city', function(done) {
    request(app)
      .get('/api/weather/simple')
      .set('Content-Type', 'application/json')
      .query({ city: 'Madrid'})
      .expect('Content-Type', /json/)
      .expect(200, function(err, res) {
        if (err) { return done(err); }
        expect(res.body.city).to.equal('Madrid');
        expect(res.body.temperature).to.not.be.null;
        expect(res.body.icon).to.not.be.null;
        done();
      });
  });
});

describe('=== API: api/weather/simple/ |city=unknownCity| ===', function() {
  this.timeout(10000)
  it('Should send 404 error code', function(done) {
    request(app)
      .get('/api/weather/simple')
      .set('Content-Type', 'application/json')
      .query({ city: 'unknownCity'})
      .expect('Content-Type', /json/)
      .expect(404, function(err, res) {
        if (err) { return done(err); }
        expect(res.body.status).to.equal('404');
        done();
      });
  });
});

describe('=== API: api/weather/complex/ |city=Paris| ===', function() {
  this.timeout(10000)
  it('Should send back a JSON complex temperature informations about a city', function(done) {
    request(app)
      .get('/api/weather/complex')
      .set('Content-Type', 'application/json')
      .query({ city: 'Paris'})
      .expect('Content-Type', /json/)
      .expect(200, function(err, res) {
        if (err) { return done(err); }
        expect(res.body.city).to.equal('Paris');
        expect(res.body.temperature).to.not.be.null;
        expect(res.body.temperature_min).to.not.be.null;
        expect(res.body.temperature_max).to.not.be.null;
        expect(res.body.icon).to.not.be.null;
        expect(res.body.description).to.not.be.null;
        expect(res.body.pressure).to.not.be.null;
        expect(res.body.humidity).to.not.be.null;
        expect(res.body.wind).to.not.be.null;
        done();
      });
  });
});

describe('=== API: api/weather/complex/ |city=Madrid| ===', function() {
  this.timeout(10000)
  it('Should send back a JSON complex temperature informations about a city', function(done) {
    request(app)
      .get('/api/weather/complex')
      .set('Content-Type', 'application/json')
      .query({ city: 'Madrid'})
      .expect('Content-Type', /json/)
      .expect(200, function(err, res) {
        if (err) { return done(err); }
        expect(res.body.city).to.equal('Madrid');
        expect(res.body.temperature).to.not.be.null;
        expect(res.body.temperature_min).to.not.be.null;
        expect(res.body.temperature_max).to.not.be.null;
        expect(res.body.icon).to.not.be.null;
        expect(res.body.description).to.not.be.null;
        expect(res.body.pressure).to.not.be.null;
        expect(res.body.humidity).to.not.be.null;
        expect(res.body.wind).to.not.be.null;
        done();
      });
  });
});

describe('=== API: api/weather/complex/ |city=London| ===', function() {
  this.timeout(10000)
  it('Should send back a JSON complex temperature informations about a city', function(done) {
    request(app)
      .get('/api/weather/complex')
      .set('Content-Type', 'application/json')
      .query({ city: 'London'})
      .expect('Content-Type', /json/)
      .expect(200, function(err, res) {
        if (err) { return done(err); }
        expect(res.body.city).to.equal('London');
        expect(res.body.temperature).to.not.be.null;
        expect(res.body.temperature_min).to.not.be.null;
        expect(res.body.temperature_max).to.not.be.null;
        expect(res.body.icon).to.not.be.null;
        expect(res.body.description).to.not.be.null;
        expect(res.body.pressure).to.not.be.null;
        expect(res.body.humidity).to.not.be.null;
        expect(res.body.wind).to.not.be.null;
        done();
      });
  });
});

describe('=== API: api/weather/complex/ |city=unknownCity| ===', function() {
  this.timeout(10000)
  it('Should send back a JSON complex temperature informations about a city', function(done) {
    request(app)
      .get('/api/weather/complex')
      .set('Content-Type', 'application/json')
      .query({ city: 'unknownCity'})
      .expect('Content-Type', /json/)
      .expect(404, function(err, res) {
        if (err) { return done(err); }
        expect(res.body.status).to.equal('404');
        done();
      });
  });
});

describe('=== API: api/weather/forecast/ |city=London| ===', function() {
  this.timeout(10000)
  it('Should send back a JSON with forecast about a city temperature informations', function(done) {
    request(app)
      .get('/api/weather/forecast')
      .set('Content-Type', 'application/json')
      .query({ city: 'London'})
      .expect('Content-Type', /json/)
      .expect(200, function(err, res) {
        if (err) { return done(err); }
        expect(res.body.city).to.equal('London');
        expect(res.body.temperatureList).to.not.be.null;
        done();
      });
  });
});

describe('=== API: api/weather/forecast/ |city=Paris| ===', function() {
  this.timeout(10000)
  it('Should send back a JSON with forecast about a city temperature informations', function(done) {
    request(app)
      .get('/api/weather/forecast')
      .set('Content-Type', 'application/json')
      .query({ city: 'Paris'})
      .expect('Content-Type', /json/)
      .expect(200, function(err, res) {
        if (err) { return done(err); }
        expect(res.body.city).to.equal('Paris');
        expect(res.body.temperatureList).to.not.be.null;
        done();
      });
  });
});

describe('=== API: api/weather/forecast/ |city=Madrid| ===', function() {
  this.timeout(10000)
  it('Should send back a JSON with forecast about a city temperature informations', function(done) {
    request(app)
      .get('/api/weather/forecast')
      .set('Content-Type', 'application/json')
      .query({ city: 'Madrid'})
      .expect('Content-Type', /json/)
      .expect(200, function(err, res) {
        if (err) { return done(err); }
        expect(res.body.city).to.equal('Madrid');
        expect(res.body.temperatureList).to.not.be.null;
        done();
      });
  });
});

describe('=== API: api/weather/forecast/ |city=unknownCity| ===', function() {
  this.timeout(10000)
  it('Should send back a JSON with forecast about a city temperature informations', function(done) {
    request(app)
      .get('/api/weather/forecast')
      .set('Content-Type', 'application/json')
      .query({ city: 'unknownCity'})
      .expect('Content-Type', /json/)
      .expect(404, function(err, res) {
        if (err) { return done(err); }
        expect(res.body.status).to.equal('404');
        done();
      });
  });
});