const app = require("../server.js")
const request = require('supertest');
const expect = require('chai').expect

describe('=== API: api/wikipedia/article |search=dofus| ===', function() {
    this.timeout(10000)
    it('Should send back a JSON with content and image of a wikipedia article', function(done) {
      request(app)
        .get('/api/wikipedia/article')
        .set('Content-Type', 'application/json')
        .query({ search: 'Dofus'})
        .expect('Content-Type', /json/)
        .expect(200, function(err, res) {
          if (err) { return done(err); }
          done();
        });
    });
});

describe('=== API: api/wikipedia/article |search=batman| ===', function() {
    this.timeout(10000)
    it('Should send back a JSON with content and image of a wikipedia article', function(done) {
      request(app)
        .get('/api/wikipedia/article')
        .set('Content-Type', 'application/json')
        .query({ search: 'batman'})
        .expect('Content-Type', /json/)
        .expect(200, function(err, res) {
          if (err) { return done(err); }
          done();
        });
    });
});

describe('=== API: api/wikipedia/article |search=pokemon| ===', function() {
    this.timeout(10000)
    it('Should send back a JSON with content and image of a wikipedia article', function(done) {
      request(app)
        .get('/api/wikipedia/article')
        .set('Content-Type', 'application/json')
        .query({ search: 'pokemon'})
        .expect('Content-Type', /json/)
        .expect(200, function(err, res) {
          if (err) { return done(err); }
          done();
        });
    });
});

describe('=== API: api/wikipedia/article |search=aaaaaaaaaaaaaaa| ===', function() {
    this.timeout(10000)
    it('Should send back a 404 erroe code', function(done) {
      request(app)
        .get('/api/wikipedia/article')
        .set('Content-Type', 'application/json')
        .query({ search: 'aaaaaaaaaaaaaaa'})
        .expect('Content-Type', /json/)
        .expect(404, function(err, res) {
          if (err) { return done(err); }
          done();
        });
    });
});

describe('=== API: api/wikipedia/article |search=bbbbbbbbbbb| ===', function() {
    this.timeout(10000)
    it('Should send back a 404 erroe code', function(done) {
      request(app)
        .get('/api/wikipedia/article')
        .set('Content-Type', 'application/json')
        .query({ search: 'bbbbbbbbbbb'})
        .expect('Content-Type', /json/)
        .expect(404, function(err, res) {
          if (err) { return done(err); }
          done();
        });
    });
});