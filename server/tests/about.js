const app = require("../server.js")
const request = require('supertest');
const expect = require('chai').expect

describe('=== API: /about.json ===', function() {
    this.timeout(10000)
    it('Should send back a JSON with informations about the dashboard services/widgets', function(done) {
      request(app)
        .get('/about.json')
        .set('Content-Type', 'application/json')
        .expect('Content-Type', /json/)
        .expect(200, function(err, res) {
          if (err) { return done(err); }
          expect(res.status).to.equal(200);
          done();
        });
    });
});