const app = require("../server.js")
const request = require('supertest');
const expect = require('chai').expect

describe('=== API: api/youtube/channel |url=https://www.youtube.com/channel/UCJt6d-PCtyQIHW2Ub0X9C0w| ===', function() {
    this.timeout(10000)
    it('Should send back a JSON with informations about a youtube channel', function(done) {
      request(app)
        .get('/api/youtube/channel')
        .set('Content-Type', 'application/json')
        .query({ url: 'https://www.youtube.com/channel/UCJt6d-PCtyQIHW2Ub0X9C0w'})
        .expect('Content-Type', /json/)
        .expect(200, function(err, res) {
          if (err) { return done(err); }
          expect(res.body.title).to.equal('Robin He');
          done();
        });
    });
});

describe('=== API: api/youtube/channel |url=https://www.youtube.com/channel/UC29ju8bIPH5as8OGnQzwJyA| ===', function() {
    this.timeout(10000)
    it('Should send back a JSON with informations about a youtube channel', function(done) {
      request(app)
        .get('/api/youtube/channel')
        .set('Content-Type', 'application/json')
        .query({ url: 'https://www.youtube.com/channel/UC29ju8bIPH5as8OGnQzwJyA'})
        .expect('Content-Type', /json/)
        .expect(200, function(err, res) {
          if (err) { return done(err); }
          expect(res.body.title).to.equal('Traversy Media');
          done();
        });
    });
});

describe('=== API: api/youtube/channel |url=https://www.youtube.com/channel/UC29ju8bIPH5as8OGnQzwJA| ===', function() {
    this.timeout(10000)
    it('Should send back a 404 error code', function(done) {
      request(app)
        .get('/api/youtube/channel')
        .set('Content-Type', 'application/json')
        .query({ url: 'https://www.youtube.com/channel/UC29ju8bIPH5as8OGnQzwJA'})
        .expect('Content-Type', /json/)
        .expect(404, function(err, res) {
          if (err) { return done(err); }
          done();
        });
    });
});

describe('=== API: api/youtube/channel |url=https://www.youtube.com/channel/UCJt6d-PCtyQIHW2Ub0X9C0| ===', function() {
    this.timeout(10000)
    it('Should send back a 404 error code', function(done) {
      request(app)
        .get('/api/youtube/channel')
        .set('Content-Type', 'application/json')
        .query({ url: 'https://www.youtube.com/channel/UCJt6d-PCtyQIHW2Ub0X9C0'})
        .expect('Content-Type', /json/)
        .expect(404, function(err, res) {
          if (err) { return done(err); }
          done();
        });
    });
});

describe('=== API: api/youtube/video |url=https://www.youtube.com/watch?v=g_aMpyMvQ9k| ===', function() {
    this.timeout(10000)
    it('Should send back a JSON with informations about a youtube video', function(done) {
      request(app)
        .get('/api/youtube/video')
        .set('Content-Type', 'application/json')
        .query({ url: 'https://www.youtube.com/watch?v=g_aMpyMvQ9k'})
        .expect('Content-Type', /json/)
        .expect(200, function(err, res) {
          if (err) { return done(err); }
          expect(res.body.title).to.equal('Escape Tutorial Hell & Utilize Them In A Better Way');
          done();
        });
    });
});

describe('=== API: api/youtube/video |url=https://www.youtube.com/watch?v=peiYvyhvk-w| ===', function() {
    this.timeout(10000)
    it('Should send back a JSON with informations about a youtube video', function(done) {
      request(app)
        .get('/api/youtube/video')
        .set('Content-Type', 'application/json')
        .query({ url: 'https://www.youtube.com/watch?v=peiYvyhvk-w'})
        .expect('Content-Type', /json/)
        .expect(200, function(err, res) {
          if (err) { return done(err); }
          expect(res.body.title).to.equal('2019 Tech Cave Tour / Home Studio Setup');
          done();
        });
    });
});

describe('=== API: api/youtube/video |url=https://www.youtube.com/watch?v=g_aMpyMvQ| ===', function() {
    this.timeout(10000)
    it('Should send back a 404 error code', function(done) {
      request(app)
        .get('/api/youtube/video')
        .set('Content-Type', 'application/json')
        .query({ url: 'https://www.youtube.com/watch?v=g_aMpyMvQ'})
        .expect('Content-Type', /json/)
        .expect(404, function(err, res) {
          if (err) { return done(err); }
          done();
        });
    });
});

describe('=== API: api/youtube/video |url=https://www.youtube.com/watch?v=peiYvyhvk-| ===', function() {
    this.timeout(10000)
    it('Should send back a 404 error code', function(done) {
      request(app)
        .get('/api/youtube/video')
        .set('Content-Type', 'application/json')
        .query({ url: 'https://www.youtube.com/watch?v=peiYvyhvk-'})
        .expect('Content-Type', /json/)
        .expect(404, function(err, res) {
          if (err) { return done(err); }
          done();
        });
    });
});