const app = require("../server.js")
const request = require('supertest');
const expect = require('chai').expect

describe('=== API: /api/news |search=gilet| ===', function() {
    this.timeout(10000)
    it('Should send back a JSON with informations about the dashboard services/widgets', function(done) {
      request(app)
        .get('/api/news')
        .set('Content-Type', 'application/json')
        .query({ search: 'gilet'})
        .expect('Content-Type', /json/)
        .expect(200, function(err, res) {
          if (err) { return done(err); }
          done();
        });
    });
});

describe('=== API: /api/news |search=foot| ===', function() {
    this.timeout(10000)
    it('Should send back a JSON with informations about the dashboard services/widgets', function(done) {
      request(app)
        .get('/api/news')
        .set('Content-Type', 'application/json')
        .query({ search: 'foot'})
        .expect('Content-Type', /json/)
        .expect(200, function(err, res) {
          if (err) { return done(err); }
          done();
        });
    });
});

describe('=== API: /api/news |search=jaune| ===', function() {
    this.timeout(10000)
    it('Should send back a JSON with informations about the dashboard services/widgets', function(done) {
      request(app)
        .get('/api/news')
        .set('Content-Type', 'application/json')
        .query({ search: 'jaune'})
        .expect('Content-Type', /json/)
        .expect(200, function(err, res) {
          if (err) { return done(err); }
          done();
        });
    });
});