const app = require("../server.js")
const request = require('supertest');
const expect = require('chai').expect

describe('=== API: /api/epitech/user |user=hyokil.kim@epitech.eu| ===', function() {
    this.timeout(10000)
    it('Should send back a JSON with informations about the student', function(done) {
      request(app)
        .get('/api/epitech/user')
        .set('Content-Type', 'application/json')
        .query({user: 'hyokil.kim@epitech.eu'})
        .expect('Content-Type', /json/)
        .expect(200, function(err, res) {
          if (err) { return done(err); }
          done();
        });
    });
});

describe('=== API: /api/epitech/user |user=vanderstichel.jean-baptiste@epitech.eu| ===', function() {
    this.timeout(10000)
    it('Should send back a JSON with informations about the student', function(done) {
      request(app)
        .get('/api/epitech/user')
        .set('Content-Type', 'application/json')
        .query({user: 'vanderstichel.jean-baptiste@epitech.eu'})
        .expect('Content-Type', /json/)
        .expect(200, function(err, res) {
          if (err) { return done(err); }
          done();
        });
    });
});

describe('=== API: /api/epitech/user |user=gregoire.loupy@epitech.eu| ===', function() {
    this.timeout(10000)
    it('Should send back a JSON with informations about the student', function(done) {
      request(app)
        .get('/api/epitech/user')
        .set('Content-Type', 'application/json')
        .query({user: 'gregoire.loupy@epitech.eu'})
        .expect('Content-Type', /json/)
        .expect(200, function(err, res) {
          if (err) { return done(err); }
          done();
        });
    });
});

describe('=== API: /api/epitech/user |user=unknownUser| ===', function() {
    this.timeout(10000)
    it('Should send back a JSON with an error', function(done) {
      request(app)
        .get('/api/epitech/user')
        .set('Content-Type', 'application/json')
        .query({user: 'unknownUser'})
        .expect('Content-Type', /json/)
        .expect(404, function(err, res) {
          if (err) { return done(err); }
          done();
        });
    });
});

describe('=== API: /api/epitech/user |user=hyokil.kimtrololol@epitech.eu| ===', function() {
    this.timeout(10000)
    it('Should send back a JSON with an error', function(done) {
      request(app)
        .get('/api/epitech/user')
        .set('Content-Type', 'application/json')
        .query({user: 'hyokil.kimtrololol@epitech.eu'})
        .expect('Content-Type', /json/)
        .expect(404, function(err, res) {
          if (err) { return done(err); }
          done();
        });
    });
});

describe('=== API: /api/epitech/binome |user=hyokil.kim@epitech.eu| ===', function() {
    this.timeout(10000)
    it('Should send back a JSON with informations about the student', function(done) {
      request(app)
        .get('/api/epitech/binome')
        .set('Content-Type', 'application/json')
        .query({user: 'hyokil.kim@epitech.eu'})
        .expect('Content-Type', /json/)
        .expect(200, function(err, res) {
          if (err) { return done(err); }
          done();
        });
    });
});

describe('=== API: /api/epitech/binome |user=vanderstichel.jean-baptiste@epitech.eu| ===', function() {
    this.timeout(10000)
    it('Should send back a JSON with informations about the student', function(done) {
      request(app)
        .get('/api/epitech/binome')
        .set('Content-Type', 'application/json')
        .query({user: 'vanderstichel.jean-baptiste@epitech.eu'})
        .expect('Content-Type', /json/)
        .expect(200, function(err, res) {
          if (err) { return done(err); }
          done();
        });
    });
});

describe('=== API: /api/epitech/binome |user=gregoire.loupy@epitech.eu| ===', function() {
    this.timeout(10000)
    it('Should send back a JSON with informations about the student', function(done) {
      request(app)
        .get('/api/epitech/binome')
        .set('Content-Type', 'application/json')
        .query({user: 'gregoire.loupy@epitech.eu'})
        .expect('Content-Type', /json/)
        .expect(200, function(err, res) {
          if (err) { return done(err); }
          done();
        });
    });
});

describe('=== API: /api/epitech/binome |user=unknownUser| ===', function() {
    this.timeout(10000)
    it('Should send back a JSON with an error', function(done) {
      request(app)
        .get('/api/epitech/binome')
        .set('Content-Type', 'application/json')
        .query({user: 'unknownUser'})
        .expect('Content-Type', /json/)
        .expect(404, function(err, res) {
          if (err) { return done(err); }
          done();
        });
    });
});

describe('=== API: /api/epitech/binome |user=hyokil.kimtrololol@epitech.eu| ===', function() {
    this.timeout(10000)
    it('Should send back a JSON with an error', function(done) {
      request(app)
        .get('/api/epitech/binome')
        .set('Content-Type', 'application/json')
        .query({user: 'hyokil.kimtrololol@epitech.eu'})
        .expect('Content-Type', /json/)
        .expect(404, function(err, res) {
          if (err) { return done(err); }
          done();
        });
    });
});

describe('=== API: /api/epitech/grade |user=hyokil.kim@epitech.eu| ===', function() {
    this.timeout(10000)
    it('Should send back a JSON with informations about the student', function(done) {
      request(app)
        .get('/api/epitech/grade')
        .set('Content-Type', 'application/json')
        .query({user: 'hyokil.kim@epitech.eu'})
        .expect('Content-Type', /json/)
        .expect(200, function(err, res) {
          if (err) { return done(err); }
          done();
        });
    });
});

describe('=== API: /api/epitech/grade |user=vanderstichel.jean-baptiste@epitech.eu| ===', function() {
    this.timeout(10000)
    it('Should send back a JSON with informations about the student', function(done) {
      request(app)
        .get('/api/epitech/grade')
        .set('Content-Type', 'application/json')
        .query({user: 'vanderstichel.jean-baptiste@epitech.eu'})
        .expect('Content-Type', /json/)
        .expect(200, function(err, res) {
          if (err) { return done(err); }
          done();
        });
    });
});

describe('=== API: /api/epitech/grade |user=gregoire.loupy@epitech.eu| ===', function() {
    this.timeout(10000)
    it('Should send back a JSON with informations about the student', function(done) {
      request(app)
        .get('/api/epitech/grade')
        .set('Content-Type', 'application/json')
        .query({user: 'gregoire.loupy@epitech.eu'})
        .expect('Content-Type', /json/)
        .expect(200, function(err, res) {
          if (err) { return done(err); }
          done();
        });
    });
});

describe('=== API: /api/epitech/grade |user=unknownUser| ===', function() {
    this.timeout(10000)
    it('Should send back a JSON with an error', function(done) {
      request(app)
        .get('/api/epitech/grade')
        .set('Content-Type', 'application/json')
        .query({user: 'unknownUser'})
        .expect('Content-Type', /json/)
        .expect(404, function(err, res) {
          if (err) { return done(err); }
          done();
        });
    });
});

describe('=== API: /api/epitech/grade |user=hyokil.kimtrololol@epitech.eu| ===', function() {
    this.timeout(10000)
    it('Should send back a JSON with an error', function(done) {
      request(app)
        .get('/api/epitech/grade')
        .set('Content-Type', 'application/json')
        .query({user: 'hyokil.kimtrololol@epitech.eu'})
        .expect('Content-Type', /json/)
        .expect(404, function(err, res) {
          if (err) { return done(err); }
          done();
        });
    });
});