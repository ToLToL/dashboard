const app = require("../server.js")
const request = require('supertest');
const expect = require('chai').expect

describe('=== API: / ===', function() {
    it('Should send back a JSON with message: Welcome', function(done) {
      request(app)
        .get('/')
        .set('Content-Type', 'application/json')
        .expect('Content-Type', /json/)
        .expect(200, function(err, res) {
          if (err) { return done(err); }
          callStatus = res.body.message;
          expect(callStatus).to.equal('Welcome');
          done();
        });
    });
});