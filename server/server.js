const express = require('express');
const mongoose = require('mongoose');
const cors = require('cors');
const flash = require('express-flash');
const session = require('express-session');
const passport = require('passport');
const swaggerJsDoc = require('swagger-jsdoc');
const swaggerUi = require('swagger-ui-express');

const app = express();

// https://swagger.io/specification/
const swaggerOptions = {
    swaggerDefinition: {
        info: {
            title: 'Dashboard API',
            version: '1.0.0',
            description: 'Dashboard API Information made by Hyokil KIM and Jean-Baptiste VANDERSTICHEL',
            contact: {
                name: 'Amazing developers'
            },
            servers: ['http://localhost:8080']
        }
    },
    apis: ['./routes/api/*.js']
};

const swaggerDocs = swaggerJsDoc(swaggerOptions)

// Middlewares
app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(swaggerDocs))
app.use(express.urlencoded({ extended: false }));
app.use(cors());
app.use(flash());
app.use(session({
    secret: 'secret',
    resave: false,
    saveUninitialized: false
}));
app.use(passport.initialize())
app.use(passport.session())

app.options('*', cors());

// Database config
const db = require('./config/keys').mongoPort;

// Connect to Mongo
mongoose
    .connect(
        db,
        { useNewUrlParser: true, useUnifiedTopology: true })
    .then(() => console.log('MongoDB connected...'))
    .catch(err => console.log(err))

// Routes
app.use('/', require('./routes/api/index'));
app.use('/api/weather', require('./routes/api/weather'));
app.use('/api/auth', require('./routes/api/auth'));
app.use('/api/stock', require('./routes/api/stock'));
app.use('/api/github', require('./routes/api/github'));
app.use('/api/epitech', require('./routes/api/epitech'));
app.use('/api/youtube', require('./routes/api/youtube'));
app.use('/api/crypto', require('./routes/api/crypto'));
app.use('/api/wikipedia', require('./routes/api/wikipedia'));
app.use('/api/news', require('./routes/api/news'));
app.use('/about.json', require('./routes/about'));

const port = process.env.PORT || 8080

app.listen(port, () => console.log(`Server started on port ${port}`));

module.exports = app;